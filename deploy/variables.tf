variable "prefix" {
  default = "dtaad"
}

variable "project" {
  default = "deeptracks-app-api-devops"
}

variable "contact" {
  default = "thecodeswitcher@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "deeptracks-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "777361135022.dkr.ecr.us-east-1.amazonaws.com/deeptracks-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "777361135022.dkr.ecr.us-east-1.amazonaws.com/deeptracks-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "spotify_client_id" {
  description = "Client ID for Spotify Dev App"
}

variable "spotify_client_secret" {
  description = "Client Secret for Spotify Dev App"
}

variable "recent_plays_lambda_address" {
  description = "Address for API route that runs lambda to get latest tracks of user"
}

variable "sqs_delayed_recent_plays_lambda_address" {
  description = "Address for API route that triggers the delayed get latest tracks of user"
}

variable "fernet_key" {
  description = "Fernet key for en/decrypting access keys for the Spotify API"
}

variable "cors_origin_whitelist" {
  description = "Comma separated list for CORS_ORIGIN_WHITELIST"
}

variable "frontend_url" {
  description = "Redirects to this url after spotify authorization"
}

variable "jira_b64" {
  description = "Jira credentials b64 encoded."
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "deeptracks.io"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
