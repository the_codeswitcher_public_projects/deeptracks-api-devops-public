# Deeptracks API Devops Repo

## Getting started

To start project, run:

```
docker-compose up
```

The API will then be available at http://127.0.0.1:8000


To run any django management commands locally

```bash
docker-compose run --rm app sh -c "python manage.py startapp spotify"
docker-compose run --rm app sh -c "python manage.py test"
docker-compose run --rm app sh -c "python manage.py createsuperuser"
```

To run terraform commands

```bash
aws-vault exec jonathan.ramirez
docker-compose -f deploy/docker-compose.yml run --rm terraform plan|apply|destroy
```

To create a super user
```commandline
ssh ec2-user@[bastion_host]

$(aws ecr get-login --no-include-email --region us-east-1)

docker run -it \
    -e DB_HOST=<DB_HOST> \
    -e DB_NAME=deeptracks \
    -e DB_USER=deeptracks \
    -e DB_PASS=<DB_PASS> \
    <ECR_REPO>:latest \
    sh -c "python manage.py wait_for_db && python manage.py createsuperuser"
```

ssh into bastion server for db
```commandline
ssh -NL 8886:[db_host]:5432 ec2-user@[bastion_host] -v
```
