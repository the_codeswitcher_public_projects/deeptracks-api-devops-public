Django>=2.1.3,<2.2.0
djangorestframework>=3.9.0,<3.10.0
psycopg2>=2.7.5,<2.8.0
Pillow>=5.3.0,<5.4.0
uwsgi>=2.0.18,<2.1.0
boto3>=1.12.0,<1.13.0
django-storages>=1.9.1,<1.10.0
black==21.12b0
requests>=2.27.1,<2.28.0
python-dotenv>=0.19.2,<0.20.0
cryptography>=36.0.2,<36.1.0
django-cors-headers>=3.3.0,<3.4.0

flake8>=3.6.0,<3.7.0
