from django.db import models
from django.contrib.auth import get_user_model
from django.core import signing
from datetime import timedelta
from django.utils import timezone

from spotify.constants import (
    SPOTIFY_ACCESS_TOKEN,
    SECRET_CHOICES,
)

from pomodoro.models import Pomodoro
import logging
import os

logger = logging.getLogger(__name__)


def spotify_file_path(instance, filename):
    """Generate file path for new spotify file"""
    return os.path.join("spotify_data/", filename)


def spotify_track_play_path(
    instance,
    filename,
):
    """Generate file path for new spotify trackplay file"""

    file_type_str = "_".join(instance.file_type.split(" ")).lower()

    ext = filename.split(".")[-1]
    pomodoro = getattr(instance, "pomodoro", None)
    pomodoro_prefix = f"pomodoro{pomodoro.id}/" if pomodoro else ""
    ts = (
        instance.created_at.strftime("%Y%m%dT%H%M")
        if instance.file_type != "Profile Tracks"
        else "daily_file"
    )

    filename = f"{pomodoro_prefix}{ts}.{ext}"
    trackplay_path = os.path.join(f"user{instance.user.id}/{file_type_str}", filename)

    logger.warning(f"trackplay_path: {trackplay_path}")

    return spotify_file_path(instance, trackplay_path)


class SpotifySecret(models.Model):

    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    profile = models.ForeignKey(
        "SpotifyProfile", on_delete=models.CASCADE, null=True, blank=True
    )
    secret_type = models.CharField(max_length=50, choices=SECRET_CHOICES)
    secret = models.TextField()
    expires_in = models.IntegerField(null=True, blank=True)
    is_revoked = models.BooleanField(default=False)
    is_encrypted = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        state_adding = self._state.adding
        if state_adding:
            raw_secret = signing.dumps({"raw_secret": self.secret})
            self.secret = raw_secret
            self.is_encrypted = True
        super().save(*args, **kwargs)

    @property
    def decrypted_secret(self):
        """Returns the decrypted secret"""
        return signing.loads(self.secret)["raw_secret"]

    @property
    def is_expired(self):
        if self.secret_type == SPOTIFY_ACCESS_TOKEN:
            return timezone.now() > self.created_at + timedelta(
                0, self.expires_in - 100
            )


class SpotifyProfile(models.Model):

    display_name = models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField()
    profile_id = models.CharField(max_length=255, db_index=True)
    uri = models.CharField(max_length=255, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return f"{self.display_name}<{self.profile_id}>"


class UserSpotifyProfiles(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, db_index=True)
    profile = models.ForeignKey(SpotifyProfile, on_delete=models.CASCADE, db_index=True)


class SpotifyPlaylist(models.Model):
    profile = models.ForeignKey(SpotifyProfile, on_delete=models.CASCADE, db_index=True)
    playlist_id = models.CharField(max_length=255, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class SpotifyTrack(models.Model):
    MODE_CHOICES = [(1, "MAJOR"), (0, "MINOR")]
    track_id = models.CharField(max_length=255, db_index=True)
    album_id = models.CharField(max_length=255, db_index=True, null=True, blank=True)
    uri = models.CharField(max_length=255, db_index=True)
    acousticness = models.FloatField(null=True, blank=True)
    danceability = models.FloatField(null=True, blank=True)
    duration_ms = models.IntegerField(null=True, blank=True)
    energy = models.FloatField(null=True, blank=True)
    instrumentalness = models.FloatField(null=True, blank=True)
    key = models.IntegerField(null=True, blank=True)
    liveness = models.FloatField(null=True, blank=True)
    loudness = models.FloatField(null=True, blank=True)
    mode = models.IntegerField(choices=MODE_CHOICES, null=True, blank=True)
    speechiness = models.FloatField(null=True, blank=True)
    tempo = models.FloatField(null=True, blank=True)
    time_signature = models.IntegerField(null=True, blank=True)
    valence = models.FloatField(null=True, blank=True)
    preview_url = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class SpotifyProfileTrack(models.Model):
    """Tracks played by or saved by a profile."""

    profile = models.ForeignKey(SpotifyProfile, on_delete=models.CASCADE, db_index=True)
    track = models.ForeignKey(SpotifyTrack, on_delete=models.CASCADE, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class SpotifyTrackPlay(models.Model):
    profile = models.ForeignKey(SpotifyProfile, on_delete=models.CASCADE, db_index=True)
    track = models.ForeignKey(SpotifyTrack, on_delete=models.CASCADE, db_index=True)
    pomodoro = models.ForeignKey(
        Pomodoro, on_delete=models.CASCADE, db_index=True, null=True, blank=True
    )
    played_at = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        SpotifyProfileTrack.objects.get_or_create(
            profile=self.profile, track=self.track
        )


class SpotifyFiles(models.Model):
    FILE_TYPE_TRACKPLAY = "TrackPlays"
    FILE_TYPE_MODEL = "ML Model"
    FILE_TYPE_TRACK_AUDIO_FILE = "Track Audio File"
    FILE_TYPE_AUDIO_SPECTOGRAM = "Track Spectogram"
    FILE_TYPE_PROFILE_TRACKS = "Profile Tracks"
    FILE_TYPE_CHOICES = [
        (FILE_TYPE_TRACKPLAY, FILE_TYPE_TRACKPLAY),
        (FILE_TYPE_MODEL, FILE_TYPE_MODEL),
        (FILE_TYPE_TRACK_AUDIO_FILE, FILE_TYPE_TRACK_AUDIO_FILE),
        (FILE_TYPE_AUDIO_SPECTOGRAM, FILE_TYPE_AUDIO_SPECTOGRAM),
        (FILE_TYPE_PROFILE_TRACKS, FILE_TYPE_PROFILE_TRACKS),
    ]

    user = models.ForeignKey(
        get_user_model(), null=True, blank=True, on_delete=models.CASCADE, db_index=True
    )
    pomodoro = models.ForeignKey(
        Pomodoro, null=True, blank=True, on_delete=models.CASCADE, db_index=True
    )
    file_content = models.FileField(null=True, upload_to=spotify_track_play_path)
    file_location = models.TextField(db_index=True)
    file_type = models.CharField(
        max_length=20, db_index=True, choices=FILE_TYPE_CHOICES
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.file_content is not None and self.file_location is None:
            self.file_location = self.file_content.name
            super().save()
