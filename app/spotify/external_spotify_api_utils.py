import requests
import os
import base64
from cryptography.fernet import Fernet

TOKEN_URL = "https://accounts.spotify.com/api/token"
client_creds = (
    f"{os.environ.get('SPOTIFY_CLIENT_ID', 'changeme')}:"
    f"{os.environ.get('SPOTIFY_CLIENT_SECRET', 'changeme')}"
)
client_creds_b64 = base64.b64encode(client_creds.encode())
USER_TOKEN_HEADERS = {
    "Content-Type": "application/x-www-form-urlencoded",
    "Authorization": f"Basic {client_creds_b64.decode()}",
}


class FernetEncryptor:
    def __init__(self):
        self.fernet = Fernet(os.environ.get("FERNET_KEY", "changeme").encode("utf-8"))

    def encrypt_access_token(self, access_token: str):
        """Takes an access_token and encrypts it using the Fernet key"""
        return self.fernet.encrypt(access_token.encode("utf-8")).decode()

    def decrypt_access_token(self, encrypted_access_token: str):
        """Takes an access_token and decrypts it using the Fernet key"""
        return self.fernet.decrypt(encrypted_access_token.encode("utf-8")).decode()


class SpotifyAPIUser:
    def __init__(self, user):
        self.user = user
        self.access_tokens = {}
        self.fernet = FernetEncryptor()

    def get_access_tokens(self, encrypt_keys=False):
        """Will get access_tokens from the refresh tokens associated to the User
        using the Spotify API
        """
        refresh_tokens = self.user.spotifysecret_set.all()
        for rt in refresh_tokens:
            refresh_token = rt.decrypted_secret
            refresh_body = f"grant_type=refresh_token&refresh_token={refresh_token}"
            user_auth_token_from_refresh_res = requests.post(
                TOKEN_URL, data=refresh_body, headers=USER_TOKEN_HEADERS
            )
            if user_auth_token_from_refresh_res.status_code == 200:
                access_token = user_auth_token_from_refresh_res.json()["access_token"]
                if encrypt_keys:
                    access_token = self.fernet.encrypt_access_token(access_token)
                self.access_tokens[rt.profile.id] = access_token
            elif (
                "refresh token revoked"
                in str(user_auth_token_from_refresh_res.json()).lower()
            ):
                rt.delete()
        return self.access_tokens
