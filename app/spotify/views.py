from django.http import HttpResponseRedirect
from rest_framework import viewsets, mixins, status
from spotify.custom_exceptions import (
    NoUserProvided,
    NoPomodoroProvided,
)
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from django.contrib.auth import get_user_model
from rest_framework.views import APIView
from rest_framework.exceptions import APIException
from django.db import transaction
from django.db.models import Q

from spotify.models import (
    SpotifySecret,
    SpotifyProfile,
    SpotifyPlaylist,
    SpotifyTrack,
    SpotifyTrackPlay,
    UserSpotifyProfiles,
    SpotifyProfileTrack,
)
from spotify.constants import SPOTIFY_ACCESS_TOKEN, SPOTIFY_REFRESH_TOKEN
from spotify import serializers
from pomodoro.models import Pomodoro
import requests
import os
import logging
import json
import traceback
from spotify.trackplay_utils import (
    get_pomo_track_plays,
    save_trackplay_file,
    serialize_trackplays,
    serialize_profiletracks,
    save_profiletrack_file,
    SpotifyAPIHandler,
)
from spotify.access_token_utils import scope_string, TOKEN_URL, USER_TOKEN_HEADERS
from spotify.external_spotify_api_utils import SpotifyAPIUser, FernetEncryptor

logger = logging.getLogger(__name__)


class NoSpotifyProfileError(APIException):
    """User should have a spotify profile created"""

    status_code = status.HTTP_400_BAD_REQUEST


class SpotifyAPIHandlerException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST


class IsSuperUser(IsAdminUser):
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_superuser)


class BaseSpotifySecretAttrViewSet(
    viewsets.GenericViewSet,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
):
    """Base class for managing relations in the db"""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_user(self):
        if self.request.method == "GET":
            user_id = self.request.query_params.get("user")
            if not user_id:
                raise NoUserProvided("User must be provided in the query param")
        else:
            user_id = self.request.data.get("user")
            if not user_id:
                raise NoUserProvided("User must be provided in the req body")
        return get_user_model().objects.get(id=user_id)

    def get_queryset(self):
        """Return objects for the current authenticated user only"""
        queryset = self.queryset
        return queryset.filter(user=self.get_user()).order_by("-id")

    def perform_create(self, serializer):
        """Create a new object"""
        serializer.save(user=self.get_user())


class SpotifySecretViewSet(BaseSpotifySecretAttrViewSet):
    """Manage SpotifySecrets in the database"""

    queryset = SpotifySecret.objects.all()
    serializer_class = serializers.SpotifySecretSerializer


class AuthenticationView(APIView):
    @transaction.atomic
    def get(self, request, format=None):
        """Gets the access and refresh tokens after the user authorizes DeepTracks"""
        # This redirect uri must match the URI given when generating the user code.
        self.redirect_uri = (
            f"""{request.build_absolute_uri('/').strip("/")}"""
            """/api/spotify/auth/callback"""
        )
        user_code = request.query_params.get("code")
        user_id = request.query_params.get("state")
        self.auth_token_url = (
            "https://accounts.spotify.com/authorize?"
            f"client_id={os.environ.get('SPOTIFY_CLIENT_ID', 'changeme')}"
            f"&response_type=code&redirect_uri={self.redirect_uri}"
            f"&scope={scope_string}&state={user_id}"
        )
        if not user_code:
            logger.warning("user code not properly generated")
            return HttpResponseRedirect(self.auth_token_url)
        if not user_id:
            raise NoUserProvided("user_id not provided in state")
        user = get_user_model().objects.filter(id=user_id)

        if not user:
            return Response(
                {"detail": "No such user."}, status=status.HTTP_400_BAD_REQUEST
            )

        user = user.get()

        # if the user revoked access and is now granting again, clear out all tokens.
        existing_access_tokens = SpotifySecret.objects.filter(
            user=user,
        )
        if existing_access_tokens:
            existing_access_tokens.delete()

        user_token_res = self.get_user_access_tokens(user_code, USER_TOKEN_HEADERS)
        access_token = user_token_res["access_token"]
        refresh_token = user_token_res["refresh_token"]
        expires_in = user_token_res["expires_in"]

        refresh_token = SpotifySecret.objects.create(
            user=user,
            secret_type=SPOTIFY_REFRESH_TOKEN,
            is_revoked=False,
            secret=refresh_token,
        )

        access_token = SpotifySecret.objects.create(
            user=user,
            secret_type=SPOTIFY_ACCESS_TOKEN,
            secret=access_token,
            expires_in=expires_in,
        )

        profile = self.create_spotify_profile_entry(access_token, refresh_token)
        access_token.profile = profile
        access_token.save()
        refresh_token.profile = profile
        refresh_token.save()
        redirect_url = os.environ.get("FRONTEND_URL", "http://localhost:3000/dashboard")
        return HttpResponseRedirect(redirect_url)

    def get_access_from_refresh_token(self, refresh_token):
        """If a user's access token has expired, uses refresh token to get a new one"""
        refresh_body = (
            f"grant_type=refresh_token&refresh_token={refresh_token.decrypted_secret}"
        )
        user_auth_token_from_refresh_res = requests.post(
            TOKEN_URL, data=refresh_body, headers=USER_TOKEN_HEADERS
        )
        error_desc = user_auth_token_from_refresh_res.json().get("error_description")
        if error_desc == "Refresh token revoked":
            logger.warning(
                "user %s has revoked access to deeptracks", str(refresh_token.user.id)
            )
            return HttpResponseRedirect(self.auth_token_url)

        token = user_auth_token_from_refresh_res.json()["access_token"]
        expires_in = user_auth_token_from_refresh_res.json()["expires_in"]
        SpotifySecret.objects.create(
            user=refresh_token.user,
            secret_type=SPOTIFY_ACCESS_TOKEN,
            secret=token,
            expires_in=expires_in,
        )

    def create_spotify_profile_entry(
        self, access_token: SpotifySecret, refresh_token: SpotifySecret
    ):
        """Using the access code, save the spotify profile"""
        user_profile_url = "https://api.spotify.com/v1/me"

        user = access_token.user
        # do not attempt to create a profile entry if access has been revoked.
        if refresh_token.is_revoked:
            logger.warning("user %s has revoked access to deeptracks", str(user.id))
            return HttpResponseRedirect(self.auth_token_url)

        if access_token.is_expired:
            logger.warning(f"user: {user}'s access token has expired. Refreshing...")
            self.get_access_from_refresh_token(refresh_token)

        user_profile_headers = {
            "Authorization": f"Bearer {access_token.decrypted_secret}"
        }
        user_profile_res = requests.get(user_profile_url, headers=user_profile_headers)

        defaults = {
            key: user_profile_res.json()[key]
            for key in ["display_name", "email", "id", "uri"]
        }
        profile_id = defaults.pop("id")
        profile, created = SpotifyProfile.objects.get_or_create(
            profile_id=profile_id, defaults=defaults
        )
        UserSpotifyProfiles.objects.get_or_create(user=user, profile=profile)

        return profile

    def get_user_access_tokens(self, user_code, user_token_headers):
        user_token_body = (
            f"grant_type=authorization_code"
            f"&code={user_code}&redirect_uri={self.redirect_uri}"
        )
        user_auth_token_res = requests.post(
            TOKEN_URL, data=user_token_body, headers=user_token_headers
        )
        res = user_auth_token_res.json()
        if user_auth_token_res.status_code != 200:
            logger.warning(f"Failure res: {res}")
            return Response(
                {"detail": "Spotify authorization failed"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        return res


class BaseSpotifyUserObjAttrViewSet(
    viewsets.GenericViewSet,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
):
    """Base class for managing relations in the db"""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsSuperUser,)

    def get_queryset(self):
        """Return objects for the current authenticated user only"""
        queryset = self.queryset
        user_id = self.request.query_params.get("user")
        user_profiles = UserSpotifyProfiles.objects.filter(user_id=user_id)
        profiles = [up.profile for up in user_profiles]
        logger.warning(f"profiles: {profiles}")
        if not profiles:
            raise NoSpotifyProfileError("User does not have a SpotifyProfile")
        return queryset.filter(profile=profiles[0]).order_by("-id")

    def perform_create(self, serializer):
        """Create a new object"""
        user_id = self.request.query_params.get("user")
        if not user_id:
            raise NoUserProvided("No user_id provided in the query params")
        user = get_user_model().objects.get(id=user_id)

        profiles = UserSpotifyProfiles.objects.filter(user=user).order_by("-id")
        if not profiles:
            raise NoSpotifyProfileError("User does not have a SpotifyProfile")
        serializer.save(profile=profiles.first().profile)


class SpotifyPlaylistViewSet(BaseSpotifyUserObjAttrViewSet):
    """Manage Pomodoros in the database"""

    queryset = SpotifyPlaylist.objects.all()
    serializer_class = serializers.SpotifyPlaylistSerializer


class BaseSpotifyTrackAttrViewSet(
    viewsets.GenericViewSet,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
):
    """Base class for managing relations in the db"""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsSuperUser,)


class SpotifyTrackViewSet(BaseSpotifyTrackAttrViewSet):

    queryset = SpotifyTrack.objects.all()
    serializer_class = serializers.SpotifyTrackSerializer

    def create(self, request, *args, **kwargs):
        """If the track exists, then status will be 200, not 201"""
        track_id = self.request.data.get("track_id")
        updating = False
        if track_id:
            updating = self.queryset.filter(track_id=track_id).exists()
        res = super().create(request, *args, **kwargs)
        if updating:
            res.status_code = status.HTTP_200_OK
        return res


class SpotifyTrackPlayViewSet(BaseSpotifyUserObjAttrViewSet):

    queryset = SpotifyTrackPlay.objects.all()
    serializer_class = serializers.SpotifyTrackPlaySerializer

    def perform_create(self, serializer):
        """Checks if there is both a user and profile then associates a pomodoro
        if trackplay occurs within one.
        """
        user_id = self.request.query_params.get("user")
        if not user_id:
            raise NoUserProvided("No user_id provided in the query params")
        user = get_user_model().objects.get(id=user_id)

        profiles = UserSpotifyProfiles.objects.filter(user=user).order_by("-id")
        if not profiles:
            raise NoSpotifyProfileError("User does not have a SpotifyProfile")
        played_at = self.request.data["played_at"]

        or_condition = Q()
        or_condition.add(Q(**{"ended_at__gte": played_at}), Q.OR)
        or_condition.add(Q(**{"ended_at__isnull": True}), Q.OR)

        associated_pomodoros = Pomodoro.objects.filter(
            Q(created_at__lte=played_at), or_condition
        )
        if associated_pomodoros:
            serializer.save(
                profile=profiles.first().profile, pomodoro=associated_pomodoros.first()
            )
        serializer.save(profile=profiles.first().profile)

    def create(self, request, *args, **kwargs):
        """Only calls the create if the trackkplay does not exist

        SpotifyTrackPlay is not expected to need updates because it just
        keeps track of a track_id and timestamp.
        """
        request_serializer = self.get_serializer(data=request.data)
        request_serializer.is_valid(raise_exception=True)
        existing_trackplays = self.queryset.filter(**request_serializer.data)
        if existing_trackplays:
            serializer = self.serializer_class(existing_trackplays.first(), many=False)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_200_OK, headers=headers)
        return super().create(request, *args, **kwargs)


class AllTrackPlaysView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        """Get all track plays"""
        user_id = self.request.query_params.get("user")
        save_pomo_to_file = "save_file" in self.request.query_params
        force_create = "force_create" in self.request.query_params
        search_by_pomodoro = "pomodoro" in self.request.query_params

        pomodoro = None
        user = None

        if search_by_pomodoro:
            pom = request.query_params.get("pomodoro")
            pomodoro, user_trackplays = get_pomo_track_plays(pom=pom, user_id=user_id)
            user = pomodoro.user

        if user_id:
            user = get_user_model().objects.get(id=user_id)
            profiles = UserSpotifyProfiles.objects.filter(user=user).order_by("-id")
            if not profiles:
                raise NoSpotifyProfileError("User does not have a SpotifyProfile")

            user_trackplays = (
                SpotifyTrackPlay.objects.filter(profile=profiles.first().profile)
                .select_related("track")
                .all()
            )
        track_play_output = serialize_trackplays(user_trackplays)
        if save_pomo_to_file:
            trackplay_file = save_trackplay_file(
                track_play_output, user, pomodoro, force_create
            )
            return Response(
                {
                    "file_id": trackplay_file.id,
                    "file_location": trackplay_file.file_content.name,
                },
                status=status.HTTP_200_OK,
            )
        return Response({"results": track_play_output}, status=status.HTTP_200_OK)


class PomodorosWithTrackplaysMissingFiles(APIView):
    """Returns the pomodoros that have trackplays,
    which have not yet been saved to a file.
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        """Find all pomodoros without files and have trackplays"""
        pomodoros_missing_files = Pomodoro.objects.filter(
            spotifytrackplay__isnull=False, spotifyfiles__isnull=True
        )
        pomodoro_ids = [p.id for p in pomodoros_missing_files]
        return Response({"results": pomodoro_ids}, status=status.HTTP_200_OK)


class UserEncryptedAccessKeys(APIView):
    """Returns encrypted access keys for the specified user/pomodoro"""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsSuperUser,)

    def get(self, request, format=None):
        """Gets encrypted access keys to send externally
        These keys can only be decrypted with the Fernet key.
        """
        user_id = self.request.query_params.get("user")
        pom_id = self.request.query_params.get("pomodoro")
        try:
            if user_id:
                user = get_user_model().objects.get(id=user_id)
            else:
                pomodoro = Pomodoro.objects.get(id=pom_id)
                user = pomodoro.user
        except get_user_model().DoesNotExist:
            raise NoUserProvided("User does not exist")
        except Pomodoro.DoesNotExist:
            raise NoPomodoroProvided("Pomodoro does not exist")
        spotify_api_user = SpotifyAPIUser(user)
        raw_access_tokens = spotify_api_user.get_access_tokens()
        fernet = FernetEncryptor()
        encrypted_access_tokens = {
            profile_id: {
                "encrypted_access_key": fernet.encrypt_access_token(token_body),
                "user": user.id,
            }
            for profile_id, token_body in raw_access_tokens.items()
        }
        logger.warning(f"encrypted_tokens: {json.dumps(encrypted_access_tokens)}")
        return Response(
            {"access_tokens": encrypted_access_tokens}, status=status.HTTP_200_OK
        )


class UserSaveProfileTracks(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsSuperUser,)

    def get(self, request, format=None):
        """Saves an instance of SpotifyFile for all the
        SpotifyProfileTracks
        """
        user_id = self.request.query_params.get("user")
        include_ts = self.request.query_params.get("include_ts", "false") == "true"

        if not user_id:
            raise NoUserProvided("User not provided!")

        try:
            user = get_user_model().objects.get(id=user_id)
        except get_user_model().DoesNotExist:
            raise NoUserProvided("User does not exist")
        profile_tracks = SpotifyProfileTrack.objects.none()
        logger.warning(f"UserSaveProfileTracks: {user.userspotifyprofiles_set.all()}")
        for user_profile in user.userspotifyprofiles_set.all():
            profile_tracks = profile_tracks.union(
                user_profile.profile.spotifyprofiletrack_set.all()
            )
        if not profile_tracks.exists():
            return Response(
                {"detail": "User has no profile tracks"},
                status=status.HTTP_204_NO_CONTENT,
            )
        serialized_profile_tracks = serialize_profiletracks(profile_tracks)
        logger.warning(f"UserSaveProfileTracks.include_ts: {include_ts}")
        trackplay_file = save_profiletrack_file(
            serialized_profile_tracks,
            user=user,
        )

        logger.warning(
            f"trackplay_file.__dict__: {trackplay_file.file_content.__dict__}"
        )

        return Response(
            {
                "file_id": trackplay_file.id,
                "file_location": trackplay_file.file_location
                or trackplay_file.file_content.name,
            },
            status=status.HTTP_200_OK,
        )


class UserProfileSongsBaseView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsSuperUser,)

    @property
    def spotify_api_handler_method(self):
        raise NotImplementedError(
            "Implement spotify_api_handler_method()!"
            "Call a method such as get_saved_spotify_tracks()"
        )

    def get(self, request, format=None):
        user_id = self.request.query_params.get("user")
        if not user_id:
            raise NoUserProvided("User must be specified in the query params")
        try:
            user = get_user_model().objects.get(id=user_id)
        except get_user_model().DoesNotExist:
            raise get_user_model().DoesNotExist("User %s does not exist", str(user_id))
        logger.warning(f"Starting to download all songs for user {user.id}")
        for refresh_token in user.spotifysecret_set.filter(
            secret_type=SPOTIFY_REFRESH_TOKEN
        ):
            try:
                spotify_api = SpotifyAPIHandler(refresh_token=refresh_token)
                handler_method = getattr(spotify_api, self.spotify_api_handler_method)
                handler_method()
            except Exception as e:
                tb = traceback.format_exc()
                raise SpotifyAPIHandlerException(
                    "Exception raised when trying to download songs for user"
                    f"{str(user.id)}, {str(e)} Trackback: {str(tb)}"
                )

        logger.warning(f"Finished downloading all songs for user {user.id}")
        return Response(
            {"detail": f"Finished downloading all songs for user {user.id}"},
            status=status.HTTP_200_OK,
        )


class GetAndSaveProfileTracksView(UserProfileSongsBaseView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsSuperUser,)

    @property
    def spotify_api_handler_method(self):
        return "get_saved_spotify_tracks"


class GetAndSaveTracksFromSavedPlaylistsView(UserProfileSongsBaseView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsSuperUser,)

    @property
    def spotify_api_handler_method(self):
        return "get_all_user_playlist_songs"
