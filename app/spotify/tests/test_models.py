from django.test import TestCase
from django.contrib.auth import get_user_model
from spotify.models import SpotifySecret
from spotify.constants import SPOTIFY_ACCESS_TOKEN, SPOTIFY_REFRESH_TOKEN
from django.core import signing


def sample_user(email="test@deeptracks.com", password="testpass"):
    """Create a sample user"""
    return get_user_model().objects.create_user(email, password)


def encrypt_string(raw_string):
    """Encrypt a string with Signer"""
    obj = {"raw_secret": raw_string}
    return signing.dumps(obj)


def decrypt_string(enc_string):
    """Decrypt a string with Signer"""
    return signing.loads(enc_string)


class ModelTests(TestCase):
    def setUp(self):
        self.user = sample_user()
        self.raw_secret_key = "MySecretKey"
        self.secret = SpotifySecret.objects.create(
            user=self.user,
            secret_type=SPOTIFY_ACCESS_TOKEN,
            secret=self.raw_secret_key,
        )

    def test_create_secret(self):
        """Test creating a new pomodoro activity"""
        self.assertEquals(len(SpotifySecret.objects.all()), 1)

    def test_secret_encrypted(self):
        """Test that a SpotifySecret object is encrypted"""
        encrypted_string = encrypt_string(self.raw_secret_key)
        self.assertEqual(encrypted_string, self.secret.secret)
        self.assertNotEqual(self.raw_secret_key, self.secret.secret)
        self.assertEqual(self.raw_secret_key, self.secret.decrypted_secret)

    def test_secret_does_not_encrypt_twice(self):
        """Saving a change to a secret should not encrypt the secret again"""
        self.secret.secret_type = SPOTIFY_REFRESH_TOKEN
        self.secret.save()
        self.assertEqual(self.secret.decrypted_secret, self.raw_secret_key)
