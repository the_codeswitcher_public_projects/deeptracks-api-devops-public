import datetime
import pytz

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

from rest_framework.test import APIClient
from rest_framework import status

from spotify.constants import SPOTIFY_ACCESS_TOKEN, SPOTIFY_REFRESH_TOKEN
from spotify.serializers import (
    SpotifyPlaylistSerializer,
    SpotifyTrackSerializer,
    SpotifyTrackPlaySerializer,
)
from spotify.models import (
    SpotifyProfile,
    SpotifyTrack,
    SpotifyTrackPlay,
    UserSpotifyProfiles,
    SpotifyFiles,
    SpotifyProfileTrack,
)
from spotify.views import serialize_trackplays
from pomodoro.models import Pomodoro, PomodoroActivity
from utils.timestamp_utils import now_utc
import logging
import json

logger = logging.getLogger(__name__)

SPOTIFY_SECRET_URL = reverse("spotify:spotifysecret-list")
SPOTIFY_PLAYLIST_URL = reverse("spotify:spotifyplaylist-list")
SPOTIFY_TRACK_URL = reverse("spotify:spotifytrack-list")
SPOTIFY_TRACKPLAY_URL = reverse("spotify:spotifytrackplay-list")
SPOTIFY_USER_TRACKPLAY_URL = reverse("spotify:trackplays")
SPOTIFY_POMODOROS_NO_TRACKPLAY_URL = reverse("spotify:pomodoros-no-trackplay-file")
SPOTIFY_ACCESS_TOKEN_URL = reverse("spotify:access-tokens")
SPOTIFY_PROFILE_TRACKS_URL = reverse("spotify:profile-tracks")

SPOTIFY_TRACK_PAYLOAD = {
    "track_id": "1BlwjpI6kgMA31YXySMq3H",
    "album_id": "albumID",
    "uri": "spotify:track:1BlwjpI6kgMA31YXySMq3H",
    "danceability": 0.443,
    "energy": 0.122,
    "key": 4,
    "loudness": -17.245,
    "mode": 0,
    "speechiness": 0.0302,
    "acousticness": 0.919,
    "instrumentalness": 0.566,
    "liveness": 0.18,
    "valence": 0.269,
    "tempo": 110.313,
    "duration_ms": 127616,
    "preview_url": (
        "https://p.scdn.co/mp3-preview/"
        + "1c07f8102dd6d45817e9d825ab0a5f379041f53f"
        + "?cid=b91b12e476204f4f9ae494e4e37b6479"
    ),
}

SPOTIFY_TRACK_PAYLOAD2 = {
    "track_id": "1BlwjpI6kgMA31YXySMq3H",
    "album_id": "albumID",
    "uri": "spotify:track:1BlwjpI6kgMA31YXySMq3H",
    "danceability": 0.556,
    "energy": 0.133,
    "key": 4,
    "loudness": -17.568,
    "mode": 0,
    "speechiness": 0.0402,
    "acousticness": 0.909,
    "instrumentalness": 0.655,
    "liveness": 0.32,
    "valence": 0.366,
    "tempo": 110.415,
    "duration_ms": 127618,
    "preview_url": (
        "https://p.scdn.co/mp3-preview/"
        + "1c07f8102dd6d45817e9d825ab0a5f379041f53f"
        + "?cid=b91b12e476204f4f9ae494e4e37b6479"
    ),
}

SPOTIFY_TRACK_PAYLOAD3 = {
    "track_id": "otherTrackId",
    "album_id": "albumID",
    "uri": "spotify:track:otherTrackId",
    "danceability": 0.556,
    "energy": 0.133,
    "key": 4,
    "loudness": -17.568,
    "mode": 0,
    "speechiness": 0.0402,
    "acousticness": 0.909,
    "instrumentalness": 0.655,
    "liveness": 0.32,
    "valence": 0.366,
    "tempo": 110.415,
    "duration_ms": 127618,
    "preview_url": (
        "https://p.scdn.co/mp3-preview/"
        + "1c07f8102dd6d45817e9d825ab0a5f379041f53f"
        + "?cid=b91b12e476204f4f9ae494e4e37b6479"
    ),
}


def create_user(**params):
    return get_user_model().objects.create_user(**params, is_test=True)


def create_superuser():
    user = get_user_model().objects.create_superuser(
        "superuser@deeptracks.com", "test123"
    )
    return user


def create_track():
    return SpotifyTrack.objects.create(**SPOTIFY_TRACK_PAYLOAD)


def create_spotify_profile(user):
    spotify_profile = SpotifyProfile.objects.create(
        display_name="Test",
        email=user.email,
        profile_id="ProfileID",
        uri="spotify:uri:test",
    )
    return UserSpotifyProfiles.objects.create(user=user, profile=spotify_profile)


def create_trackplay(spotify_profile, track):
    return SpotifyTrackPlay.objects.create(
        **{
            "profile": spotify_profile,
            "track": track,
            "played_at": "2022-02-13T14:54:14.970Z",
        }
    )


def create_pomodoro(user, length=25, break_length=5):
    return Pomodoro.objects.create(user=user, length=length, break_length=break_length)


class PrivateSpotifyPlaylistApiTests(TestCase):
    """Test API requests that require authentication"""

    def setUp(self):
        self.user = create_user(
            email="test@deeptracks.com", password="testpass", name="name"
        )
        self.superuser = create_superuser()
        self.client = APIClient()
        self.client.force_authenticate(user=self.superuser)
        self.playlist_user_url = SPOTIFY_PLAYLIST_URL + f"?user={self.user.id}"
        self.spotify_profile = SpotifyProfile.objects.create(
            display_name="Test",
            email=self.user.email,
            profile_id="ProfileID",
            uri="spotify:uri:test",
        )
        UserSpotifyProfiles.objects.create(user=self.user, profile=self.spotify_profile)

    def test_no_profile_error(self):
        """If a user does not have a SpotifyProfile, this should fail"""
        self.spotify_profile.delete()
        self.client.force_authenticate(user=self.superuser)
        payload = {
            "playlist_id": "AwesomePlaylistID",
        }

        res = self.client.post(self.playlist_user_url, payload)
        self.assertEqual(res.status_code, 400)
        self.assertEqual(res.data, {"detail": "User does not have a SpotifyProfile"})

    def test_authentication_error(self):
        """Trying to create a SpotifyPlaylist as a non-superuser should fail"""
        self.client.force_authenticate(user=self.user)
        payload = {
            "playlist_id": "AwesomePlaylistID",
        }

        res = self.client.post(self.playlist_user_url, payload)
        self.assertEqual(res.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            res.data, {"detail": "You do not have permission to perform this action."}
        )

    def test_playlist_creation(self):
        """Tests SpotifyPlaylist creation"""
        payload = {
            "playlist_id": "AwesomePlaylistID",
        }

        res = self.client.post(self.playlist_user_url, payload)
        created_playlist = self.spotify_profile.spotifyplaylist_set.first()
        serializer = SpotifyPlaylistSerializer(created_playlist, many=False)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(res.data, serializer.data)


class PrivateSpotifyTrackApiTests(TestCase):
    """Test API requests that require authentication"""

    def setUp(self):
        self.superuser = create_superuser()
        self.user = create_user(
            email="test@deeptracks.com", password="testpass", name="name"
        )
        self.client = APIClient()
        self.client.force_authenticate(user=self.superuser)

        self.spotify_profile = SpotifyProfile.objects.create(
            display_name="Test",
            email=self.user.email,
            profile_id="ProfileID",
            uri="spotify:uri:test",
        )
        UserSpotifyProfiles.objects.create(user=self.user, profile=self.spotify_profile)
        self.payload = SPOTIFY_TRACK_PAYLOAD

    def test_authentication_error(self):
        """Trying to create a SpotifyPlaylist as a non-superuser should fail"""
        self.client.force_authenticate(user=self.user)

        res = self.client.post(SPOTIFY_TRACK_URL, self.payload)
        self.assertEqual(res.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            res.data, {"detail": "You do not have permission to perform this action."}
        )

    def test_track_creation(self):
        """Tests SpotifyTrack creation"""
        res = self.client.post(SPOTIFY_TRACK_URL, self.payload)
        created_track = SpotifyTrack.objects.first()
        serializer = SpotifyTrackSerializer(created_track, many=False)
        self.assertEqual(res.data, serializer.data)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

    def test_upsert_track(self):
        """Tests that upsert updates successfully"""
        self.client.post(SPOTIFY_TRACK_URL, self.payload)
        res = self.client.post(SPOTIFY_TRACK_URL, SPOTIFY_TRACK_PAYLOAD2)
        updated_track = SpotifyTrack.objects.first()
        serializer = SpotifyTrackSerializer(updated_track, many=False)
        self.assertEqual(len(SpotifyTrack.objects.all()), 1)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        for key in SPOTIFY_TRACK_PAYLOAD2:
            self.assertEqual(serializer.data[key], SPOTIFY_TRACK_PAYLOAD2[key])


class PrivateSpotifyTrackPlayApiTests(TestCase):
    """Test API requests that require authentication"""

    def setUp(self):
        self.superuser = create_superuser()
        self.user = create_user(
            email="test@deeptracks.com", password="testpass", name="name"
        )
        self.client = APIClient()
        self.client.force_authenticate(user=self.superuser)
        self.track = create_track()
        self.spotify_profile = create_spotify_profile(self.user).profile
        self.payload = {
            "profile": self.spotify_profile,
            "track": self.track.id,
            "played_at": "2022-02-13T14:54:14.970Z",
        }
        self.url = SPOTIFY_TRACKPLAY_URL + f"?user={self.user.id}"

    def test_authentication_error(self):
        """Trying to create a SpotifyTrackPlay as a non-superuser should fail"""
        self.client.force_authenticate(user=self.user)

        res = self.client.post(SPOTIFY_TRACKPLAY_URL, self.payload)
        self.assertEqual(res.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            res.data, {"detail": "You do not have permission to perform this action."}
        )

    def test_trackplay_creation(self):
        """Tests SpotifyTrackPlay creation"""
        res = self.client.post(self.url, self.payload)
        created_trackplay = self.spotify_profile.spotifytrackplay_set.first()
        serializer = SpotifyTrackPlaySerializer(created_trackplay, many=False)
        self.assertEqual(res.data, serializer.data)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

    def test_get_or_create_trackplay(self):
        """Tests that get/creates successfully"""
        self.client.post(self.url, self.payload)
        res = self.client.post(self.url, self.payload)
        self.assertEqual(len(self.spotify_profile.spotifytrackplay_set.all()), 1)
        retrieved_trackplay = self.spotify_profile.spotifytrackplay_set.first()
        serializer = SpotifyTrackPlaySerializer(retrieved_trackplay, many=False)
        self.assertEqual(res.data, serializer.data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_get_list_of_pomodoros_with_trackplays_missing_files(self):
        """A get request to this route should return a list of pomodoro
        ids that don't have an associated trackplay file.
        """
        pomodoro = create_pomodoro(self.user)
        SpotifyTrackPlay.objects.create(
            **{
                "profile": self.spotify_profile,
                "track": self.track,
                "played_at": pytz.utc.localize(datetime.datetime.now()),
                "pomodoro": pomodoro,
            }
        )
        pomodoro2 = create_pomodoro(self.user)
        SpotifyTrackPlay.objects.create(
            **{
                "profile": self.spotify_profile,
                "track": self.track,
                "played_at": pytz.utc.localize(datetime.datetime.now()),
                "pomodoro": pomodoro2,
            }
        )

        pomodoro3 = create_pomodoro(self.user)
        SpotifyTrackPlay.objects.create(
            **{
                "profile": self.spotify_profile,
                "track": self.track,
                "played_at": pytz.utc.localize(datetime.datetime.now()),
                "pomodoro": pomodoro3,
            }
        )
        self.assertEqual(len(Pomodoro.objects.all()), 3)

        self.client.get(
            SPOTIFY_USER_TRACKPLAY_URL + f"?pomodoro={pomodoro3.id}" + "&save_file"
        )
        pomodoros_no_file = self.client.get(SPOTIFY_POMODOROS_NO_TRACKPLAY_URL)
        expected_output = {"results": [pomodoro.id, pomodoro2.id]}
        for pomodoro in pomodoros_no_file.data["results"]:
            assert pomodoro in expected_output["results"]

    def test_link_trackplay_to_pom(self):
        """Tests that trackplays that happen within a pomodoro are linked to it"""
        pre_pom_payload = SpotifyTrackPlaySerializer(
            data={
                "profile": self.spotify_profile,
                "track": self.track.id,
                "played_at": pytz.utc.localize(datetime.datetime.now()),
            }
        )
        pre_pom_payload.is_valid(raise_exception=True)
        self.client.post(self.url, pre_pom_payload.data)
        pomodoro = create_pomodoro(self.user)
        post_pom_payload = SpotifyTrackPlaySerializer(
            data={
                "profile": self.spotify_profile,
                "track": self.track.id,
                "played_at": pytz.utc.localize(datetime.datetime.now()),
            }
        )
        post_pom_payload.is_valid(raise_exception=True)
        self.client.post(self.url, post_pom_payload.data)

        self.assertEqual(len(self.spotify_profile.spotifytrackplay_set.all()), 2)
        self.assertEqual(
            len(self.spotify_profile.spotifytrackplay_set.filter(pomodoro=pomodoro)), 1
        )


class PrivateSpotifyAllUserTrackPlayApiTests(TestCase):
    """Test API requests that require authentication"""

    def setUp(self):
        self.superuser = create_superuser()
        self.user = create_user(
            email="test@deeptracks.com", password="testpass", name="name"
        )
        self.client = APIClient()
        self.client.force_authenticate(user=self.superuser)
        self.track = SpotifyTrack.objects.create(**SPOTIFY_TRACK_PAYLOAD)
        self.spotify_profile = SpotifyProfile.objects.create(
            display_name="Test",
            email=self.user.email,
            profile_id="ProfileID",
            uri="spotify:uri:test",
        )
        UserSpotifyProfiles.objects.create(user=self.user, profile=self.spotify_profile)
        self.trackplay = SpotifyTrackPlay.objects.create(
            **{
                "profile": self.spotify_profile,
                "track": self.track,
                "played_at": "2022-02-13T14:54:14.970Z",
            }
        )

        self.url = SPOTIFY_USER_TRACKPLAY_URL + f"?user={self.user.id}"

    def test_authentication_error(self):
        """Trying to create a SpotifyTrackPlay as a non-superuser should fail"""
        self.client.force_authenticate(user=self.user)

        res = self.client.get(SPOTIFY_TRACKPLAY_URL)
        self.assertEqual(res.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            res.data, {"detail": "You do not have permission to perform this action."}
        )

    def test_get_all_user_trackplay(self):
        """Tests getting all SpotifyTrackPlays"""
        res = self.client.get(self.url)
        user_trackplays = (
            SpotifyTrackPlay.objects.filter(profile=self.spotify_profile)
            .select_related("track")
            .all()
        )
        track_play_output = serialize_trackplays(user_trackplays)
        self.assertEqual({"results": track_play_output}, res.data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)


class PrivateSpotifyAllPomodoroTrackPlayApiTests(TestCase):
    """Test API requests that require authentication"""

    def setUp(self):
        self.maxDiff = None
        self.superuser = create_superuser()
        self.user = create_user(
            email="test@deeptracks.com", password="testpass", name="name"
        )
        self.client = APIClient()
        self.client.force_authenticate(user=self.superuser)
        self.track = SpotifyTrack.objects.create(**SPOTIFY_TRACK_PAYLOAD)
        self.spotify_profile = SpotifyProfile.objects.create(
            display_name="Test",
            email=self.user.email,
            profile_id="ProfileID",
            uri="spotify:uri:test",
        )
        UserSpotifyProfiles.objects.create(user=self.user, profile=self.spotify_profile)

        self.pomodoro = Pomodoro.objects.create(
            user=self.user, length=25, break_length=5
        )
        self.pomodoro.created_at = "2022-02-13T14:50:14.970Z"
        self.pomodoro.save()
        pomodoro_end = PomodoroActivity.objects.create(
            pomodoro=self.pomodoro, activity_type=PomodoroActivity.ACTIVITY_END
        )
        pomodoro_end.created_at = "2022-02-13T15:15:14.970Z"
        pomodoro_end.save()
        for track_play_time in [
            "2022-02-13T14:54:14.970Z",
            "2022-02-13T14:59:14.970Z",
            "2022-02-13T15:36:14.970Z",
        ]:
            SpotifyTrackPlay.objects.create(
                **{
                    "profile": self.spotify_profile,
                    "track": self.track,
                    "played_at": track_play_time,
                }
            )

        self.url = SPOTIFY_USER_TRACKPLAY_URL + f"?user={self.user.id}"

    def test_authentication_error(self):
        """Trying to access this route without authentication should raise an error."""
        self.client.force_authenticate(user=None)

        res = self.client.get(self.url + f"&pomodoro={self.pomodoro.id}" + "&save_file")
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            res.data, {"detail": "Authentication credentials were not provided."}
        )

    def test_upload_all_track_plays_to_file(self):
        """If specified in query params, will upload trackplays to file"""
        res = self.client.get(self.url + f"&pomodoro={self.pomodoro.id}" + "&save_file")
        created_file = SpotifyFiles.objects.first()
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(
            res.data,
            {
                "file_id": created_file.id,
                "file_location": created_file.file_content.name,
            },
        )
        with created_file.file_content.open("r") as f:
            file_contents = json.loads(f.read())["data"]
        user_trackplays = serialize_trackplays(
            self.spotify_profile.spotifytrackplay_set.filter(
                played_at__range=(self.pomodoro.started_at, self.pomodoro.ended_at)
            )
        )
        for trackplay in user_trackplays:
            assert (
                trackplay in file_contents
            ), f"trackplay {trackplay} not in file_contents"

    def test_upload_all_track_plays_to_file_by_pom_id(self):
        """If specified in query params, will upload trackplays to file.
        Should be able to just take a single pomodoro id without the user_id
        """
        res = self.client.get(
            SPOTIFY_USER_TRACKPLAY_URL + f"?pomodoro={self.pomodoro.id}" + "&save_file"
        )
        created_file = SpotifyFiles.objects.first()
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(
            res.data,
            {
                "file_id": created_file.id,
                "file_location": created_file.file_content.name,
            },
        )
        with created_file.file_content.open("r") as f:
            file_contents = json.loads(f.read())["data"]
        user_trackplays = serialize_trackplays(
            self.spotify_profile.spotifytrackplay_set.filter(
                played_at__range=(self.pomodoro.started_at, self.pomodoro.ended_at)
            )
        )
        self.assertEqual(file_contents, user_trackplays)

    def test_profile_tracks_route_superuser_only(self):
        self.client.force_authenticate(user=self.user)
        res = self.client.get(SPOTIFY_PROFILE_TRACKS_URL + f"?user={self.user.id}")
        self.assertEqual(res.status_code, status.HTTP_403_FORBIDDEN)

    def test_profile_tracks_route(self):
        """Tests the functionality that saves all Trackplays of a user's
        Spotify profile daily
        SPOTIFY_PROFILE_TRACKS_URL
        """

        today = now_utc().date()
        tomorrow = today + datetime.timedelta(1)
        today_start = datetime.datetime.combine(today, datetime.time())
        today_end = datetime.datetime.combine(tomorrow, datetime.time())

        files = SpotifyFiles.objects.filter(
            file_type=SpotifyFiles.FILE_TYPE_PROFILE_TRACKS,
            user=self.user,
            created_at__gte=today_start,
            created_at__lte=today_end,
        )
        self.assertEqual(files.exists(), False)
        track = SpotifyTrack.objects.create(**SPOTIFY_TRACK_PAYLOAD3)
        create_trackplay(self.spotify_profile, track)

        SpotifyProfileTrack.objects.create(profile=self.spotify_profile, track=track)

        res = self.client.get(SPOTIFY_PROFILE_TRACKS_URL + f"?user={self.user.id}")

        files = SpotifyFiles.objects.filter(
            file_type=SpotifyFiles.FILE_TYPE_PROFILE_TRACKS,
            user=self.user,
            created_at__gte=today_start,
            created_at__lte=today_end,
        )
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(files.count(), 1)


class PrivateSpotifySecretApiTests(TestCase):
    """Test API requests that require authentication"""

    def setUp(self):
        self.user = create_user(
            email="test@deeptracks.com", password="testpass", name="name"
        )
        self.superuser = create_superuser()
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)

    def test_create_spotify_secret(self):
        """Test creating spotify secret entry with authenticated user"""

        payload = {
            "user": self.user.id,
            "secret_type": SPOTIFY_ACCESS_TOKEN,
            "secret": "mySuperSecretAccessToken",
            "expires_in": 10,
        }

        res = self.client.post(SPOTIFY_SECRET_URL, payload)
        new_access_token = self.user.spotifysecret_set.first()

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(self.user.spotifysecret_set.all()), 1)
        self.assertNotEqual(new_access_token.secret, payload["secret"])
        self.assertEqual(new_access_token.secret_type, payload["secret_type"])
        self.assertEqual(new_access_token.expires_in, payload["expires_in"])

    def test_spotify_refresh_token_user_spot_auth_true(self):
        """Creating a spotify refresh token should make the user property
        spotify_authorized True
        """

        payload = {
            "user": self.user.id,
            "secret_type": SPOTIFY_REFRESH_TOKEN,
            "secret": "mySuperSecretAccessToken",
            "expires_in": 10,
        }
        self.client.post(SPOTIFY_SECRET_URL, payload)
        self.assertTrue(self.user.spotify_authorized)

    def test_access_tokens_only_superusers(self):
        """Test that only super users can access the endpoint"""
        res = self.client.get(f"{SPOTIFY_ACCESS_TOKEN_URL}?user={self.user.id}")
        self.assertEqual(res.status_code, status.HTTP_403_FORBIDDEN)
