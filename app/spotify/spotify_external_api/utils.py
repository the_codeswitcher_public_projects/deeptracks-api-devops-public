from django.contrib.auth import get_user_model
from spotify.constants import SPOTIFY_REFRESH_TOKEN
from logging import getLogger
from typing import List, Union
import requests
import base64
import os


class UserRevokedSpotifyPermissions(Exception):
    pass


logger = getLogger(__name__)

TOKEN_URL = "https://accounts.spotify.com/api/token"
USER_PROFILE_URL = "https://api.spotify.com/v1/me"
client_creds = (
    f"{os.environ.get('SPOTIFY_CLIENT_ID','changeme')}:"
    f"{os.environ.get('SPOTIFY_CLIENT_SECRET','changeme')}"
)
client_creds_b64 = base64.b64encode(client_creds.encode())

USER_TOKEN_HEADERS = {
    "Content-Type": "application/x-www-form-urlencoded",
    "Authorization": f"Basic {client_creds_b64.decode()}",
}


def get_access_token_from_refresh(user_id: int):
    """Given a user ID, gets a new access_token
    from using the user's refresh token.
    """
    access_tokens = []
    try:
        user = get_user_model().objects.get(id=user_id)
    except get_user_model().DoesNotExist:
        raise get_user_model().DoesNotExist(f"user {user_id} does not exist")

    refresh_tokens = user.spotifysecret_set.filter(
        secret_type=SPOTIFY_REFRESH_TOKEN,
    )

    try:
        for refresh_token in refresh_tokens:
            refresh_body = f"grant_type=refresh_token&refresh_token={refresh_token}"
            user_auth_token_from_refresh_res = requests.post(
                TOKEN_URL, data=refresh_body, headers=USER_TOKEN_HEADERS
            )

            if (
                user_auth_token_from_refresh_res.status_code == 400
                and "refresh token revoked"
                in str(user_auth_token_from_refresh_res.json()).lower()
            ):
                logger.warning(
                    f"User {user_id} has revoked Spotify access to Deeptracks"
                )
                raise UserRevokedSpotifyPermissions(
                    f"User {user_id} has revoked Spotify access to Deeptracks"
                )

            access_tokens.append(
                user_auth_token_from_refresh_res.json()["access_token"]
            )
    finally:
        return access_tokens


def get_latest_trackplays_for_user(access_token: Union[str, List[str]]):
    """Given an access_token or list of access_tokens"""
