from rest_framework import serializers
from spotify.models import (
    SpotifySecret,
    SpotifyPlaylist,
    SpotifyTrack,
    SpotifyTrackPlay,
    SpotifyProfileTrack,
)
from django.contrib.auth import get_user_model


class SpotifySecretSerializer(serializers.ModelSerializer):
    """Serializer for SpotifySecret objects"""

    user = serializers.PrimaryKeyRelatedField(
        many=False, queryset=get_user_model().objects.all()
    )

    class Meta:
        model = SpotifySecret
        fields = (
            "id",
            "user",
            "secret_type",
            "expires_in",
            "created_at",
            "updated_at",
        )
        read_only_fields = ("id",)


class SpotifyPlaylistSerializer(serializers.ModelSerializer):
    """Serializer for SpotifyPlaylist objects"""

    class Meta:
        model = SpotifyPlaylist
        fields = (
            "id",
            "profile",
            "playlist_id",
            "created_at",
            "updated_at",
        )
        read_only_fields = (
            "id",
            "profile",
        )


class SpotifyTrackSerializer(serializers.ModelSerializer):
    """Serializer for SpotifyTrack objects"""

    def create(self, validated_data):
        track_id = validated_data.pop("track_id")
        spotify_track, created = SpotifyTrack.objects.update_or_create(
            track_id=track_id, defaults=validated_data
        )
        return spotify_track

    class Meta:
        model = SpotifyTrack
        fields = (
            "id",
            "track_id",
            "album_id",
            "uri",
            "acousticness",
            "danceability",
            "duration_ms",
            "energy",
            "instrumentalness",
            "key",
            "liveness",
            "loudness",
            "mode",
            "speechiness",
            "tempo",
            "time_signature",
            "valence",
            "preview_url",
            "created_at",
            "updated_at",
        )
        read_only_fields = ("id",)


class SpotifyTrackPlaySerializer(serializers.ModelSerializer):
    """Serializer for SpotifyTrackPlay"""

    class Meta:
        model = SpotifyTrackPlay
        fields = (
            "id",
            "track",
            "played_at",
            "created_at",
            "updated_at",
        )
        read_only_fields = ("id",)


class SpotifyProfileTrackSerializer(serializers.ModelSerializer):
    """Serializer for SpotifyProfileTrack"""

    class Meta:
        model = SpotifyProfileTrack
        fields = (
            "id",
            "track",
            "profile",
            "created_at",
            "updated_at",
        )
        read_only_fields = ("id",)
