import os
import base64

scopesArray = [
    "playlist-read-private",
    "playlist-read-collaborative",
    "playlist-modify-public",
    "playlist-modify-private",
    "user-read-email",
    "user-follow-read",
    "user-library-read",
    "user-library-modify",
    #     "user-read-private",
    "user-top-read",
    "user-read-playback-state",
    "user-read-recently-played",
    "user-read-currently-playing",
]
scope_string = "%20".join(scopesArray)

TOKEN_URL = "https://accounts.spotify.com/api/token"
SPOTIFY_CLIENT_ID = os.environ.get("SPOTIFY_CLIENT_ID", "changeme")
SPOTIFY_CLIENT_SECRET = os.environ.get("SPOTIFY_CLIENT_SECRET", "changeme")
CLIENT_CREDS = f"{SPOTIFY_CLIENT_ID}:{SPOTIFY_CLIENT_SECRET}"

CLIENT_CREDS_B64 = base64.b64encode(CLIENT_CREDS.encode())
USER_TOKEN_HEADERS = {
    "Content-Type": "application/x-www-form-urlencoded",
    "Authorization": f"Basic {CLIENT_CREDS_B64.decode()}",
}
