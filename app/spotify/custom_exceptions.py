from rest_framework import status
from rest_framework.exceptions import APIException


class NoUserProvided(APIException):
    """User must be specified in the request body / query params"""

    status_code = status.HTTP_400_BAD_REQUEST


class NoPomodoroProvided(APIException):
    """Pomodoro must be specified in the request query params"""

    status_code = status.HTTP_400_BAD_REQUEST


class UserHasNoPomodoros(APIException):
    """User does not have any pomodoros"""

    status_code = status.HTTP_400_BAD_REQUEST
