SPOTIFY_ACCESS_TOKEN = "ACCESS"
SPOTIFY_REFRESH_TOKEN = "REFRESH"

SECRET_CHOICES = (
    (SPOTIFY_ACCESS_TOKEN, "Spotify user's access token"),
    (SPOTIFY_REFRESH_TOKEN, "Spotify user's refresh token"),
)
