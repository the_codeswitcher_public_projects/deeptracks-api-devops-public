from django.contrib import admin
from spotify.models import (
    SpotifySecret,
    SpotifyTrack,
    SpotifyTrackPlay,
    SpotifyFiles,
    SpotifyProfileTrack,
    SpotifyProfile,
    SpotifyPlaylist,
)

# Register your models here.
for model in [
    SpotifySecret,
    SpotifyTrack,
    SpotifyTrackPlay,
    SpotifyFiles,
    SpotifyProfileTrack,
    SpotifyProfile,
    SpotifyPlaylist,
]:
    admin.site.register(model)
