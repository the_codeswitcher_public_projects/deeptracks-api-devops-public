from django.urls import path, include
from rest_framework.routers import DefaultRouter

from spotify import views

router = DefaultRouter()
router.register("secret", views.SpotifySecretViewSet)
router.register("playlist", views.SpotifyPlaylistViewSet)
router.register("track", views.SpotifyTrackViewSet)
router.register("trackplay", views.SpotifyTrackPlayViewSet)

app_name = "spotify"

urlpatterns = [
    path("", include(router.urls)),
    path("auth/callback", views.AuthenticationView.as_view()),
    path("trackplays", views.AllTrackPlaysView.as_view(), name="trackplays"),
    path(
        "pomodoros-no-trackplay-file",
        views.PomodorosWithTrackplaysMissingFiles.as_view(),
        name="pomodoros-no-trackplay-file",
    ),
    path(
        "access-tokens", views.UserEncryptedAccessKeys.as_view(), name="access-tokens"
    ),
    path(
        "profile-tracks", views.UserSaveProfileTracks.as_view(), name="profile-tracks"
    ),
    path(
        "save-profile-tracks",
        views.GetAndSaveProfileTracksView.as_view(),
        name="save-profile-tracks",
    ),
    path(
        "save-profile-playlist-tracks",
        views.GetAndSaveTracksFromSavedPlaylistsView.as_view(),
        name="save-profile-playlist-tracks",
    ),
]
