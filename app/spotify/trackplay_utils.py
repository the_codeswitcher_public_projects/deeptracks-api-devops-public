from spotify.custom_exceptions import (
    NoPomodoroProvided,
    NoUserProvided,
    UserHasNoPomodoros,
)
from django.contrib.auth import get_user_model
from spotify.models import (
    SpotifyTrackPlay,
    SpotifyFiles,
    SpotifyTrack,
    SpotifyProfileTrack,
    SpotifyPlaylist,
)
from pomodoro.models import Pomodoro
from spotify.models import spotify_track_play_path
from django.core.files.base import File
from typing import List
from spotify import serializers
import tempfile
import json
import logging
import datetime
import requests
import time
from spotify.external_spotify_api_utils import USER_TOKEN_HEADERS
from utils.timestamp_utils import now_utc

logger = logging.getLogger(__name__)


def get_pomo_track_plays(pom: str = None, user_id: str = None):
    """Save specific pomodoro track plays to .csv"""

    if not pom:
        raise NoPomodoroProvided(
            "pomodoro id or 'LATEST' must be provided in the query params"
        )
    if pom == "LATEST":
        if not user_id:
            raise NoUserProvided(
                "User id must be specified in the query params if"
                " not specifying pomodoro_id or LATEST"
            )
        try:
            user = get_user_model().objects.get(id=int(user_id))
        except get_user_model().DoesNotExist:
            raise NoUserProvided("User %s does not exist", user_id)

        pomodoros = user.pomodoro_set.order_by("-id")
        if not pomodoros:
            raise UserHasNoPomodoros("User %s has no pomodoros", str(user))
        pomodoro = pomodoros.first()
    else:
        try:
            pom_id = int(pom)
        except ValueError:
            raise NoPomodoroProvided(
                "Integer id of pomodoro must be provided in query params"
            )

        try:
            pomodoro = Pomodoro.objects.get(id=pom_id)
        except Pomodoro.DoesNotExist:
            raise UserHasNoPomodoros("No such pomodoro of id %s exists", pom)
    trackplays_within_pom = SpotifyTrackPlay.objects.filter(
        played_at__range=(pomodoro.started_at, pomodoro.ended_at)
    )
    return pomodoro, trackplays_within_pom


def save_track_children_file(
    children: List[dict],
    user: get_user_model() = None,
    pomodoro: Pomodoro = None,
    force_create: bool = False,
    file_type: str = SpotifyFiles.FILE_TYPE_TRACKPLAY,
    file_name: str = "",
):
    trackplay_file, created = SpotifyFiles.objects.get_or_create(
        user=user, pomodoro=pomodoro, file_type=file_type
    )
    if created or force_create:
        with tempfile.NamedTemporaryFile(suffix=".json", mode="wb+") as temp_csv:
            temp_csv.write(json.dumps({"data": children}).encode())
            filename = spotify_track_play_path(trackplay_file, file_name)
            logger.warning(f"filename: {filename}")
            trackplay_file.file_content = File(temp_csv)
            trackplay_file.save()
    return trackplay_file


def save_trackplay_file(
    trackplays: List[dict],
    user: get_user_model() = None,
    pomodoro: Pomodoro = None,
    force_create: bool = False,
):
    return save_track_children_file(
        trackplays,
        user=user,
        pomodoro=pomodoro,
        force_create=force_create,
        file_type=SpotifyFiles.FILE_TYPE_TRACKPLAY,
    )


def save_profiletrack_file(
    profiletracks: List[dict],
    user: get_user_model() = None,
):
    today = now_utc().date()
    tomorrow = today + datetime.timedelta(1)
    today_start = datetime.datetime.combine(today, datetime.time())
    today_end = datetime.datetime.combine(tomorrow, datetime.time())

    profile_track_file, created = SpotifyFiles.objects.get_or_create(
        user=user,
        file_type=SpotifyFiles.FILE_TYPE_PROFILE_TRACKS,
        created_at__gte=today_start,
        created_at__lte=today_end,
    )

    with tempfile.NamedTemporaryFile(suffix=".json", mode="wb+") as temp_csv:
        temp_csv.write(json.dumps({"data": profiletracks}).encode())
        filename = spotify_track_play_path(profile_track_file, "")
        logger.warning(f"filename: {filename}")
        profile_track_file.file_content = File(temp_csv)
        profile_track_file.save()
    return profile_track_file


def serialize_track_children(track_children, serializer):
    """Serializes an iterable of child objs
    of SpotifyTrack including the child and
    SpotifyTrack's attrs
    """
    track_children_output = []
    for tc in track_children:
        serialized_track = serializers.SpotifyTrackSerializer(tc.track, many=False)
        serialized_trackplay = serializer(tc, many=False)
        track_child_dict = serialized_trackplay.data
        track_child_dict.update(serialized_track.data)
        track_children_output.append(track_child_dict)
    return track_children_output


def serialize_trackplays(trackplays):
    """Serialize an iterable of SpotifyTrackPlay
    Combines them with the SpotifyTrack information
    """
    return serialize_track_children(trackplays, serializers.SpotifyTrackPlaySerializer)


def serialize_profiletracks(profile_tracks):
    """Serializes an iterable of SpotifyProfileTrack
    Combines them with the SpotifyTrack info
    """
    return serialize_track_children(
        profile_tracks, serializers.SpotifyProfileTrackSerializer
    )


class SpotifyAPIException(Exception):
    pass


class SpotifyAPIHandler:
    def __init__(self, refresh_token):
        self.refresh_token = refresh_token
        self.profile = refresh_token.profile
        self.user = self.profile.userspotifyprofiles_set.first().user
        self.user_profile_headers = {
            "Authorization": f"Bearer {self.get_access_token()}"
        }

    def get_access_token(self):
        refresh_body = (
            "grant_type=refresh_token&"
            f"refresh_token={self.refresh_token.decrypted_secret}"
        )
        token_from_refresh_res = requests.post(
            "https://accounts.spotify.com/api/token",
            data=refresh_body,
            headers=USER_TOKEN_HEADERS,
        )
        if token_from_refresh_res.status_code != 200:
            raise SpotifyAPIException(
                "SpotifyAPI issue when getting access tokens for %s: %s",
                str(self.user),
                str(token_from_refresh_res.__dict__),
            )

        return token_from_refresh_res.json()["access_token"]

    def get_or_create_spotifytracks(self, track_payloads):
        for track_payload in track_payloads:
            track_id = track_payload.pop("track_id")
            if track_id:
                try:
                    db_track, _ = SpotifyTrack.objects.get_or_create(
                        track_id=track_id, defaults=track_payload
                    )
                except SpotifyTrack.MultipleObjectsReturned:
                    duplicate_tracks = SpotifyTrack.objects.filter(track_id=track_id)
                    raise SpotifyTrack.MultipleObjectsReturned(
                        "There are multiple objects for %s: %s",
                        str(track_id),
                        str(duplicate_tracks),
                    )
                SpotifyProfileTrack.objects.get_or_create(
                    profile=self.profile, track=db_track
                )
            else:
                logger.warning(
                    "This track does not have an id. Will not get/create it: %s",
                    str(track_payload.get("uri")),
                )

    def get_track_audio_features(self, track_id):
        """For getting a single track's audio features
        Use get_tracks_audio_features when retreiving the
        features for more than 10 tracks.
        """
        audio_features_res = requests.get(
            f"https://api.spotify.com/v1/audio-features/{track_id}",
            headers=self.user_profile_headers,
        )
        return audio_features_res

    def get_tracks_audio_features(self, track_ids: List[str]):
        """Will bulk retrieve the audio features"""
        track_ids = [track_id for track_id in track_ids if track_id is not None]
        track_ids_string = "%2C".join(track_ids)
        audio_features_res = requests.get(
            f"https://api.spotify.com/v1/audio-features/?ids={track_ids_string}",
            headers=self.user_profile_headers,
        )
        return audio_features_res

    def create_track_payloads(self, tracks: List[dict]):
        """For creating multiple track payloads"""
        non_null_tracks = [track for track in tracks if track["track"] is not None]
        track_ids = [track["track"]["id"] for track in non_null_tracks]

        audio_features_res = self.get_tracks_audio_features(track_ids)

        if (
            audio_features_res.status_code == 429
            and "api rate limit excited" in str(audio_features_res.json()).lower()
        ):
            logger.warning("Retrying request for %s after 5 seconds", str(track_ids))
            time.sleep(5)
            audio_features_res = self.get_tracks_audio_features(track_ids)
        audio_features = audio_features_res.json().get("audio_features", {})
        track_payloads = []
        for track in non_null_tracks:
            uri = track["track"]["uri"]
            track_id = track["track"]["id"]
            preview_url = track["track"]["preview_url"]
            album_id = track["track"].get("album", {}).get("id")
            track_audio_features_list = (
                list(
                    filter(
                        lambda af: af["uri"] == uri if af is not None else {},
                        audio_features,
                    )
                )
                or [{}]
            )
            track_audio_features = track_audio_features_list[0]
            if not track_audio_features:
                logger.warning(f"Could not get the audio features for {track_id}")
            track_payloads.append(
                {
                    "track_id": track_id,
                    "album_id": album_id,
                    "uri": uri,
                    "acousticness": track_audio_features.get("acousticness"),
                    "danceability": track_audio_features.get("danceability"),
                    "duration_ms": track_audio_features.get("duration_ms"),
                    "energy": track_audio_features.get("energy"),
                    "instrumentalness": track_audio_features.get("instrumentalness"),
                    "key": track_audio_features.get("key"),
                    "liveness": track_audio_features.get("liveness"),
                    "loudness": track_audio_features.get("loudness"),
                    "mode": track_audio_features.get("mode"),
                    "speechiness": track_audio_features.get("speechiness"),
                    "tempo": track_audio_features.get("tempo"),
                    "time_signature": track_audio_features.get("time_signature"),
                    "valence": track_audio_features.get("valence"),
                    "preview_url": preview_url,
                }
            )
        return track_payloads

    def chunk_tracks(self, tracks: List[str], chunk_size=100):
        total_ids = len(tracks)
        total_processed = 0
        output_chunks = []
        current_chunk = []

        for track in tracks:
            if len(current_chunk) >= chunk_size:
                output_chunks.append(current_chunk)
                current_chunk = [track]
            else:
                current_chunk.append(track)
            total_processed += 1
            if total_processed >= total_ids and len(current_chunk) > 0:
                output_chunks.append(current_chunk)
        return output_chunks

    def batch_create_spotifytrack_payloads(self, tracks, chunk_size=100):
        """Chunk up the tracks, create payloads for each chunk"""
        track_chunks = self.chunk_tracks(tracks, chunk_size)
        payloads = []
        for chunk in track_chunks:
            payloads += self.create_track_payloads(chunk)
        return payloads

    def create_track_payload(self, track):
        track_id = track["track"]["id"]
        uri = track["track"]["uri"]
        album_id = track["track"].get("album", {}).get("id")
        preview_url = track["track"]["preview_url"]
        audio_features_res = self.get_track_audio_features(track_id)
        audio_features = audio_features_res.json()
        if not audio_features.get("uri"):
            logger.warning(
                f"https://api.spotify.com/v1/audio-features/{track_id}"
                "could not get audio features!"
            )
            logger.warning(
                f"Response from Spotify headers: {audio_features_res.headers}"
            )
            logger.warning(f"Response from Spotify: {audio_features}")
        return {
            "track_id": track_id,
            "album_id": album_id,
            "uri": uri,
            "acousticness": audio_features.get("acousticness"),
            "danceability": audio_features.get("danceability"),
            "duration_ms": audio_features.get("duration_ms"),
            "energy": audio_features.get("energy"),
            "instrumentalness": audio_features.get("instrumentalness"),
            "key": audio_features.get("key"),
            "liveness": audio_features.get("liveness"),
            "loudness": audio_features.get("loudness"),
            "mode": audio_features.get("mode"),
            "speechiness": audio_features.get("speechiness"),
            "tempo": audio_features.get("tempo"),
            "time_signature": audio_features.get("time_signature"),
            "valence": audio_features.get("valence"),
            "preview_url": preview_url,
        }

    def get_all_items(self, start_url):
        user_tracks_res = requests.get(start_url, headers=self.user_profile_headers)
        if user_tracks_res.status_code > 299:
            exception = (
                f"Request to {str(start_url)} was not successful"
                f"{str(user_tracks_res.json())}"
            )
            raise SpotifyAPIException(exception)
        try:
            tracks = user_tracks_res.json()["items"]
        except KeyError:
            raise KeyError(f"'items' key is not in {user_tracks_res.json()}")

        next_url = user_tracks_res.json().get("next")
        while next_url:
            next_tracks_res = requests.get(next_url, headers=self.user_profile_headers)
            tracks += next_tracks_res.json()["items"]
            next_url = next_tracks_res.json()["next"]

        return tracks

    def get_user_playlists(self):
        """Get all playlists associated with a spotify profile"""
        user_playlists_url = "https://api.spotify.com/v1/me/playlists?limit=50"
        return self.get_all_items(user_playlists_url)

    def get_songs_for_playlist(self, playlist_id):
        start_url = (
            f"https://api.spotify.com/v1/playlists/{playlist_id}/tracks?limit=50"
        )
        return self.get_all_items(start_url=start_url)

    def get_all_user_playlist_songs(self):
        user_playlists = self.get_user_playlists()
        logger.warning(f"Getting all songs in user {self.user.id}'s playlists")
        for playlist in user_playlists:
            playlist_id = playlist["id"]
            SpotifyPlaylist.objects.get_or_create(
                profile=self.profile, playlist_id=playlist_id
            )
            playlist_songs = self.get_songs_for_playlist(playlist_id)
            logger.warning(f"Saving all songs in playlist {playlist_id}")
            track_payloads = self.batch_create_spotifytrack_payloads(playlist_songs)
            self.get_or_create_spotifytracks(track_payloads)

    def get_saved_spotify_tracks(self):
        user_tracks_url = "https://api.spotify.com/v1/me/tracks?limit=50"
        logger.warning(f"Getting all saved tracks for profile!")
        all_profile_tracks = self.get_all_items(user_tracks_url)
        logger.warning(
            f"Finished retrieving all tracks from Spotify for {self.user.id}"
        )
        track_payloads = self.batch_create_spotifytrack_payloads(all_profile_tracks)
        self.get_or_create_spotifytracks(track_payloads)
        logger.warning(
            f"Finished getting and saving all tracks from Spotify for {self.user}"
        )
