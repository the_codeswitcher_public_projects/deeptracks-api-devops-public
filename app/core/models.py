from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
)
from spotify.constants import SPOTIFY_ACCESS_TOKEN, SPOTIFY_REFRESH_TOKEN


class UserManager(BaseUserManager):
    def create_user(self, email, password=None, **extra_fields):
        """Creates and saves a new user"""
        if not email:
            raise ValueError("Users must have an email address")
        user = self.model(email=self.normalize_email(email), **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, password):
        """Creates and saves a new super user"""
        user = self.create_user(email, password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)

        return user


class User(AbstractBaseUser, PermissionsMixin):
    """Custom user model that suppors using email instead of username"""

    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_test = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = "email"

    @property
    def access_token(self):
        access_tokens = self.spotifysecret_set.filter(secret_type=SPOTIFY_ACCESS_TOKEN)
        if access_tokens:
            return access_tokens.latest("id")

    @property
    def refresh_token(self):
        refresh_tokens = self.spotifysecret_set.filter(
            secret_type=SPOTIFY_REFRESH_TOKEN,
        )
        if refresh_tokens:
            return refresh_tokens.latest("id")

    @property
    def spotify_authorized(self):
        return self.refresh_token is not None
