from django.test import TestCase
from utils.timestamp_utils import add_to_iso_string


class TimestampUtilsTests(TestCase):
    def test_add_to_iso_string(self):
        """Tests that an input iso_formatted string
        and timedelta value returns the correct value
        """
        input_iso_str = "2022-02-13T14:54:14Z"
        result = add_to_iso_string(input_iso_str, minutes=5)
        self.assertEqual(result, "2022-02-13T14:59:14Z")
