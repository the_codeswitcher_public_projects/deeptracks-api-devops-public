import datetime
import logging

logger = logging.getLogger(__name__)


def now_utc():
    """returns the current ts in UTC"""
    dt = datetime.datetime.now(datetime.timezone.utc)
    return dt.replace(tzinfo=datetime.timezone.utc)


def add_to_iso_string(iso_string: str, return_type: str = "str", **timedelta_kwargs):
    """
    Takes an ISOFormat string, adds a specific timedelta to it,
    and returns the result as an ISO string

    :param iso_string: an ISO string of a timezone aware datetime
    :param return_type: str ["str", "datetime"] dictates
    if str or datetime instance is returned
    :param timedelta_kwargs: kwargs that will go to datetime.timedelta
    """
    VALID_RETURN_TYPES = ["str", "datetime"]
    if return_type not in VALID_RETURN_TYPES:
        logger.warning(
            "return_type param must be in this list %s", str(VALID_RETURN_TYPES)
        )
        return_type = "str"
    default_kwargs = {
        "days": 0,
        "seconds": 0,
        "microseconds": 0,
        "milliseconds": 0,
        "minutes": 0,
        "hours": 0,
        "weeks": 0,
    }
    remainder = {}
    for kwarg, value in timedelta_kwargs.items():
        if kwarg in default_kwargs:
            default_kwargs[kwarg] = value
        else:
            remainder[kwarg] = value

    result = datetime.datetime.fromisoformat(
        iso_string.replace("Z", "+00:00")
    ) + datetime.timedelta(**default_kwargs)
    if return_type == "str":
        return result.strftime("%Y-%m-%dT%H:%M:%SZ")
    return result
