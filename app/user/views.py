from django.contrib.auth import get_user_model
from rest_framework import generics, authentication, permissions
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings
from rest_framework.authtoken.models import Token
from rest_framework import status
from rest_framework.parsers import MultiPartParser

import requests
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.views import APIView


from user.serializers import UserSerializer, AuthTokenSerializer
import logging
import os

logger = logging.getLogger(__name__)


class MissingBugSummary(APIException):
    pass


class NoSuchUser(APIException):
    pass


class NoFileAttached(APIException):
    pass


class NoIssueSpecified(APIException):
    pass


class CreateUserView(generics.CreateAPIView):
    """Create a new user in the system"""

    serializer_class = UserSerializer

    def post(self, request, *args, **kwargs):
        res = super().post(request, *args, **kwargs)
        if res.status_code == 201:
            user = get_user_model().objects.get(id=res.data["id"])
            token, created = Token.objects.get_or_create(user=user)
            return Response(
                {
                    "token": token.key,
                    "id": user.id,
                    "spotify_authorized": user.spotify_authorized,
                }
            )
        return res


class CreateTokenView(ObtainAuthToken):
    """Create a new auth token for user"""

    serializer_class = AuthTokenSerializer
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


class ManageUserView(generics.RetrieveUpdateAPIView, generics.RetrieveDestroyAPIView):
    """Manage the authenticated user"""

    serializer_class = UserSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self):
        """Retrieve and return authentication user"""
        return self.request.user


class UserReportBug(APIView):
    """Sends a user reported bug to the Jira API deeptracks Bug project"""

    def post(self, request, format=None):
        """Gets encrypted access keys to send externally
        These keys can only be decrypted with the Fernet key.
        """
        logger.warning(f"Incoming bug report: {request.data}")
        project_key = request.data.get("project_key", "BUG")
        headers = {
            "Authorization": f"Basic {os.environ.get('JIRA_B64')}",
        }
        try:
            summary = request.data["summary"]
        except KeyError:
            raise MissingBugSummary("A short summary of the bug is required")
        description = request.data.get("description", "")
        user_id = request.data.get("user")
        user = None
        if user_id:
            try:
                user = get_user_model().objects.get(id=user_id)
            except get_user_model().DoesNotExist:
                raise NoSuchUser(f"User {user_id} does not exist!")
        reported_by = f"\nReported by {user.email}" if user else ""
        base_body = {
            "fields": {
                "summary": summary,
                "issuetype": {"id": "10002"},
                "project": {"key": project_key},
                "description": {
                    "type": "doc",
                    "version": 1,
                    "content": [
                        {
                            "type": "paragraph",
                            "content": [
                                {"text": description + reported_by, "type": "text"}
                            ],
                        }
                    ],
                },
            }
        }
        res = requests.post(
            "https://deeptracks.atlassian.net/rest/api/3/issue/",
            json=base_body,
            headers=headers,
        )
        response = str(res)
        try:
            response = res.json()
        except Exception:
            pass
        return Response({"response_from_jira": response}, status=status.HTTP_200_OK)


class UserJiraImage(APIView):
    """Sends a user reported bug to the Jira API deeptracks Bug project"""

    parser_classes = [MultiPartParser]

    def post(self, request, format=None):
        """Gets encrypted access keys to send externally
        These keys can only be decrypted with the Fernet key.
        """

        headers = {
            "Authorization": f"Basic {os.environ.get('JIRA_B64')}",
            "X-Atlassian-Token": "nocheck",
        }

        try:
            issue_key = request.data["issue_key"]
            if not issue_key:
                raise NoIssueSpecified("Null issue_key passed in.")
        except KeyError:
            raise NoIssueSpecified("Issue key must be included")
        try:
            file_obj = request.FILES
            logger.warning(f"file_obj: {file_obj}")
        except KeyError:
            raise NoFileAttached("File must be attached.")
        jira_request_url = (
            "https://deeptracks.atlassian.net/rest/api/3/issue/"
            + f"{issue_key}/attachments/"
        )
        logger.warning(f"Posting to {jira_request_url}")
        res = requests.post(
            jira_request_url,
            files=file_obj,
            headers=headers,
        )
        response = str(res)
        try:
            response = res.json()
        except Exception:
            logger.warning(f"response: {res.__dict__}")
        return Response({"response_from_jira": response}, status=status.HTTP_200_OK)
