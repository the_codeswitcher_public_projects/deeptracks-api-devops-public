from django.contrib import admin
from pomodoro.models import Pomodoro, PomodoroActivity

# Register your models here.
admin.site.register(Pomodoro)
admin.site.register(PomodoroActivity)
