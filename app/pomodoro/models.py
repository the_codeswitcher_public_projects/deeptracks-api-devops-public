from django.db import models
from django.contrib.auth import get_user_model
import logging

logger = logging.getLogger(__name__)


class PostEndActivityException(Exception):
    """Creating an activity after a pom has ended raises an exception"""

    pass


class Pomodoro(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    length = models.PositiveIntegerField()
    break_length = models.PositiveIntegerField()
    seconds_left = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    ended_at = models.DateTimeField(null=True, blank=True)

    @property
    def started_at(self):
        return self.created_at

    @property
    def latest_activity(self):
        if self.pomodoroactivity_set.all():
            return self.pomodoroactivity_set.latest("id")

    @property
    def is_paused(self):
        return self.latest_activity.activity_type == PomodoroActivity.ACTIVITY_PAUSE

    @property
    def time_left(self):
        if self.ended_at:
            self.seconds_left = 0
            self.save()
        return self.seconds_left

    def save(self, *args, **kwargs):
        state_adding = self._state.adding
        super().save(*args, **kwargs)
        if state_adding:
            PomodoroActivity.objects.create(
                pomodoro=self, activity_type=PomodoroActivity.ACTIVITY_START
            )


class PomodoroActivity(models.Model):
    ACTIVITY_START = "START"
    ACTIVITY_END = "END"
    ACTIVITY_PAUSE = "PAUSE"
    ACTIVITY_CHOICES = (
        (ACTIVITY_START, ACTIVITY_START),
        (ACTIVITY_END, ACTIVITY_END),
        (ACTIVITY_PAUSE, ACTIVITY_PAUSE),
    )

    pomodoro = models.ForeignKey(Pomodoro, on_delete=models.CASCADE)
    activity_type = models.CharField(max_length=20, choices=ACTIVITY_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        if (
            self.pomodoro.latest_activity
            and self.pomodoro.latest_activity.activity_type == self.activity_type
        ):
            logger.error("Duplicate activity creation attempted")
            return
        if self.pomodoro.ended_at:
            logger.error("Cannot create any activity after pomodoro has ended.")
            return
        super().save(*args, **kwargs)
        if not self.pomodoro.seconds_left:
            self.pomodoro.seconds_left = self.pomodoro.length * 60
            self.pomodoro.save()

        if self.activity_type == self.ACTIVITY_END:
            self.pomodoro.ended_at = self.created_at
            self.pomodoro.seconds_left = 0
            self.pomodoro.save()

        if self.activity_type == self.ACTIVITY_PAUSE:
            start_activities = self.pomodoro.pomodoroactivity_set.filter(
                activity_type=PomodoroActivity.ACTIVITY_START
            ).order_by("-id")
            last_start = start_activities.filter(id__lt=self.id).first()
            time_elapsed = round(
                (self.created_at - last_start.created_at).total_seconds(), 0
            )
            self.pomodoro.seconds_left -= time_elapsed
            self.pomodoro.save()

    class Meta:
        verbose_name_plural = "Activities"
