from rest_framework import serializers
from pomodoro.models import Pomodoro, PomodoroActivity


class PomodoroSerializer(serializers.ModelSerializer):
    """Serializer for Pomodoro objects"""

    time_left = serializers.ReadOnlyField()
    is_paused = serializers.ReadOnlyField()

    class Meta:
        model = Pomodoro
        fields = (
            "id",
            "length",
            "break_length",
            "time_left",
            "is_paused",
            "started_at",
            "ended_at",
            "created_at",
            "updated_at",
        )
        read_only_fields = ("id",)


class PomodoroActivitySerializer(serializers.ModelSerializer):
    """Serializer for PomodoroActivity objects"""

    pomodoro = serializers.PrimaryKeyRelatedField(
        many=False, queryset=Pomodoro.objects.all()
    )

    class Meta:
        model = PomodoroActivity
        fields = (
            "id",
            "pomodoro",
            "activity_type",
            "created_at",
            "updated_at",
        )
        read_only_fields = ("id",)
