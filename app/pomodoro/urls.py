from django.urls import path, include
from rest_framework.routers import DefaultRouter

from pomodoro import views

router = DefaultRouter()
router.register("pomodoro", views.PomodoroViewSet)
router.register("pomodoroactivity", views.PomodoroActivityViewSet)

app_name = "pomodoro"

urlpatterns = [
    path("", include(router.urls)),
    path("superuser", views.PomodoroSuperuserRoute.as_view(), name="superuser"),
    path("latest", views.LatestPomodoroRoute.as_view(), name="latest"),
    path(
        "cache-timeleft", views.CacheSecondsLeftRoute.as_view(), name="cache-timeleft"
    ),
]
