from rest_framework import viewsets, mixins, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response

from rest_framework.exceptions import APIException
from rest_framework.views import APIView

from pomodoro.models import Pomodoro, PomodoroActivity
from pomodoro import serializers

from django.shortcuts import redirect

import logging
from spotify.external_spotify_api_utils import SpotifyAPIUser
from spotify.trackplay_utils import save_trackplay_file
from spotify.models import SpotifyTrackPlay
import json
import requests
import os
import datetime

log = logging.getLogger(__name__)


class IsSuperUser(IsAdminUser):
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_superuser)


class PomodoroActivityException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    pass


class PostEndActivityException(PomodoroActivityException):
    """Creating an activity after a pom has ended raises an exception"""

    pass


class MissingPomodoroException(PomodoroActivityException):
    """GET to pomodoroactivity without pomodoro query param fails"""

    pass


class DuplicateActivityException(PomodoroActivityException):
    """Attempting to create a duplicate activity raises an exception"""

    pass


class PomodoroEndActivityResponse(Response):
    """Send an API call that triggers and Airflow dag"""

    def close(self):
        super().close()

        activity_type = self.data["activity_type"]

        if self.status_code == 201 and activity_type in [
            PomodoroActivity.ACTIVITY_END,
            PomodoroActivity.ACTIVITY_PAUSE,
        ]:
            pomodoro_activity = PomodoroActivity.objects.get(id=self.data["id"])
            pomodoro = Pomodoro.objects.get(id=self.data["pomodoro"])
            recent_plays_lambda_address = os.environ.get(
                "RECENT_PLAYS_LAMBDA_ADDRESS", "changeme"
            )
            sqs_delayed_recent_plays_lambda_address = os.environ.get(
                "SQS_DELAYED_RECENT_PLAYS_LAMBDA_ADDRESS", "changeme"
            )
            user = pomodoro.user
            if not user.is_test:
                if activity_type == PomodoroActivity.ACTIVITY_END:
                    spotify_user = SpotifyAPIUser(user)
                    access_tokens = spotify_user.get_access_tokens(encrypt_keys=True)
                    if recent_plays_lambda_address != "changeme":
                        for profile_id, token in access_tokens.items():
                            res = requests.post(
                                recent_plays_lambda_address,
                                json={
                                    profile_id: {
                                        "encrypted_access_key": token,
                                        "user": pomodoro.user.id,
                                    }
                                },
                            )
                            try:
                                log.warning(
                                    "posted access key to Recent plays lambda"
                                    f"and got this response {json.dumps(res.json())}"
                                )
                            except Exception:
                                log.warning(
                                    "posted access key to Recent plays lambda"
                                    f"and got this response {str(res)}"
                                )
                            user_spotify_profiles = user.userspotifyprofiles_set.all()
                            trackplays = SpotifyTrackPlay.objects.none()
                            profile_trackplays = [
                                usp.profile.spotifytrackplay_set.all()
                                for usp in user_spotify_profiles
                            ]
                            save_trackplay_file(trackplays.union(*profile_trackplays))
                else:
                    if sqs_delayed_recent_plays_lambda_address != "changeme":
                        log.warning(
                            "Getting all trackplays from the "
                            f"Spotify api for Pomodoro {pomodoro}"
                        )
                        # send a request to the API Gateway for delayed recent plays
                        dueDate = pomodoro_activity.created_at + datetime.timedelta(
                            minutes=pomodoro.length
                        )
                        payload = {
                            "pomodoro": pomodoro.id,
                            "user": pomodoro.user.id,
                            "dueDate": dueDate.strftime("%Y-%m-%dT%H:%M:%SZ"),
                        }
                        log.warning(
                            "posting to Delayed Recent"
                            f"Plays StepFunc {json.dumps(payload)}"
                        )
                        res = requests.post(
                            sqs_delayed_recent_plays_lambda_address,
                            json=payload,
                        )
                        try:
                            result_body = json.dumps(res.json())
                        except Exception:
                            result_body = str(res)
                        log.warning(
                            "posted pomodoro payload to Delayed Recent Plays"
                            f"SQS and got this response {result_body}"
                        )
            else:
                log.warning(f"A test is running, do not run the external API logic.")


def redirect_homepage_view(request):
    response = redirect("https://google.com/")
    return response


def check_query_params(required_params=[]):
    def Inner(func):
        def wrapper(instance, *args, **kwargs):
            for key in required_params:
                if not instance.request.query_params.get(key):
                    raise MissingPomodoroException(
                        "Required param {} is missing".format(key)
                    )
            return func(instance, *args, **kwargs)

        return wrapper

    return Inner


class BasePomodoroAttrViewSet(
    viewsets.GenericViewSet,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
):
    """Base class for managing relations in the db"""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        """Return objects for the current authenticated user only"""
        queryset = self.queryset
        return queryset.filter(user=self.request.user).order_by("-id")

    def perform_create(self, serializer):
        """Create a new object"""
        serializer.save(user=self.request.user)


class PomodoroViewSet(BasePomodoroAttrViewSet):
    """Manage Pomodoros in the database"""

    queryset = Pomodoro.objects.all()
    serializer_class = serializers.PomodoroSerializer


class PomodoroActivityViewSet(BasePomodoroAttrViewSet):
    """Manage Pomodoro Activities in the database"""

    serializer_class = serializers.PomodoroActivitySerializer
    queryset = PomodoroActivity.objects.all()

    @check_query_params(["pomodoro"])
    def get_queryset(self):
        """Return objects that only pertain to the pomodoro specified in the body"""
        pomodoro = self.request.query_params.get("pomodoro")
        queryset = self.queryset
        return queryset.filter(pomodoro__id=int(pomodoro))

    def perform_create(self, serializer):
        """Create a new PomodoroActivity"""
        pomodoro = self.request.data.get("pomodoro")
        activity_type = self.request.data.get("activity_type")
        if not pomodoro:
            return Response(
                {"msg": "No pomodoro specified in the request body"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        pom = Pomodoro.objects.get(id=pomodoro)
        if pom.ended_at:
            raise PostEndActivityException(
                "Cannot create another activity for an ended Pomodoro."
            )
        if pom.latest_activity and pom.latest_activity.activity_type == activity_type:
            raise DuplicateActivityException(
                "Cannot create 2 {} activities in a row!".format(activity_type)
            )
        serializer.save()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return PomodoroEndActivityResponse(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )


class PomodoroSuperuserRoute(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsSuperUser,)

    def get(self, request):
        pomodoro = request.query_params.get("pomodoro")
        if not pomodoro:
            raise MissingPomodoroException("parameter 'pomodoro' not provided")
        try:
            retrieved_pomodoro = Pomodoro.objects.get(id=int(pomodoro))
            serializer = serializers.PomodoroSerializer(retrieved_pomodoro, many=False)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        except Pomodoro.DoesNotExist:
            raise Pomodoro.DoesNotExist(f"Pomodoro {pomodoro} does not exist")


class LatestPomodoroRoute(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        active_only = "active_only" in request.query_params
        user_pomodoros = Pomodoro.objects.filter(user=request.user).order_by("-id")
        latest = (
            user_pomodoros.filter(ended_at__isnull=True).first()
            if active_only
            else user_pomodoros.first()
        )
        serializer = serializers.PomodoroSerializer(latest, many=False)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class CacheSecondsLeftRoute(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        pomodoro_id = request.query_params.get("pomodoro")
        seconds_left = request.query_params.get("seconds_left")
        pomodoro = Pomodoro.objects.get(id=pomodoro_id)
        pomodoro.seconds_left = int(seconds_left)
        pomodoro.save()
        serializer = serializers.PomodoroSerializer(pomodoro, many=False)
        return Response(data=serializer.data, status=status.HTTP_200_OK)
