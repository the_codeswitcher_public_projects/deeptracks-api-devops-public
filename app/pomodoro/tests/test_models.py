from django.test import TestCase
from django.contrib.auth import get_user_model
from pomodoro.models import Pomodoro, PomodoroActivity


def sample_user(email="test@deeptracks.com", password="testpass"):
    """Create a sample user"""
    return get_user_model().objects.create_user(email, password)


def sample_pomodoro(user):
    """Create a sample pomodoro"""
    return Pomodoro.objects.create(user=user, length=25, break_length=5)


class ModelTests(TestCase):
    def setUp(self):
        self.user = sample_user()
        self.pom = sample_pomodoro(self.user)

    def test_create_pomodoro(self):
        """Test creating a new pomodoro with the sample user"""
        self.assertEqual(self.pom.user, self.user)
        self.assertTrue(self.pom.length, 25)
        self.assertTrue(self.pom.break_length, 5)

    def test_create_pomodoro_activity(self):
        """Test creating a new pomodoro activity"""
        PomodoroActivity.objects.create(
            pomodoro=self.pom, activity_type=PomodoroActivity.ACTIVITY_END
        )
        self.assertEqual(len(self.pom.pomodoroactivity_set.all()), 2)

    def test_proper_activity_flow(self):
        """Tests that duplicate activities cannot be created.

        e.g. After a PAUSE, only a START or END can be created
        """
        original_latest_activity = self.pom.latest_activity
        PomodoroActivity.objects.create(
            **{"pomodoro": self.pom, "activity_type": PomodoroActivity.ACTIVITY_START}
        )

        self.assertEqual(self.pom.latest_activity, original_latest_activity)
