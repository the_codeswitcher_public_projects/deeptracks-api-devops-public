from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

from rest_framework.test import APIClient
from rest_framework import status
from rest_framework.renderers import JSONRenderer

from pomodoro.models import Pomodoro, PomodoroActivity
from pomodoro.serializers import PomodoroSerializer, PomodoroActivitySerializer
from utils.timestamp_utils import now_utc
import logging
import json
import time

logger = logging.getLogger(__name__)

POM_URL = reverse("pomodoro:pomodoro-list")
POM_ACTIVITY_URL = reverse("pomodoro:pomodoroactivity-list")
POM_SUPERUSER_URL = reverse("pomodoro:superuser")
POM_LATEST_URL = reverse("pomodoro:latest")
POM_CACHE_TIMELEFT_URL = reverse("pomodoro:cache-timeleft")


def create_user(**params):
    return get_user_model().objects.create_user(**params, is_test=True)


def create_superuser():
    user = get_user_model().objects.create_superuser(
        "superuser@deeptracks.com", "test123"
    )
    return user


def create_pomodoro(user, length=25, break_length=5):
    return Pomodoro.objects.create(user=user, length=length, break_length=break_length)


class PrivatePomodoroApiTests(TestCase):
    """Test API requests that require authentication"""

    def setUp(self):
        self.user = create_user(
            email="test@deeptracks.com", password="testpass", name="name"
        )
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)

    def test_create_pomodoro(self):
        """Test creating pomodoro with authenticated user"""
        payload = {"length": 25, "break_length": 5}

        res = self.client.post(POM_URL, payload)
        user_pom = self.user.pomodoro_set.first()

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(user_pom.length, 25)
        self.assertEqual(user_pom.break_length, 5)

    def test_pomodoro_user_filter(self):
        """Test that get only retrieves the pomodoros of the authenticated user"""
        Pomodoro.objects.create(
            **{"user_id": self.user.id, "length": 25, "break_length": 5}
        )
        Pomodoro.objects.create(
            **{"user_id": self.user.id, "length": 60, "break_length": 10}
        )

        other_user = create_user(
            email="test2@deeptracks.com", password="testpass", name="name2"
        )

        Pomodoro.objects.create(
            **{"user_id": other_user.id, "length": 35, "break_length": 7}
        )

        res = self.client.get(POM_URL)
        self.assertEqual(self.user.pomodoro_set.all().count(), 2)

        pomodoros = Pomodoro.objects.filter(user=self.user).order_by("-id")
        serializer = PomodoroSerializer(pomodoros, many=True)

        self.assertEqual(res.status_code, status.HTTP_200_OK)

        comparison_fields = [
            "id",
            "length",
            "created_at",
        ]
        for index, datum in enumerate(res.data):
            serializer_datum = serializer.data[index]
            for field in comparison_fields:
                res_value = datum[field]
                serializer_value = serializer_datum[field]
                assert res_value == serializer_value, (
                    f"Pomodoro {datum['id']}.{field} in res"
                    f"{res_value} serializer {serializer_value}"
                )
        self.assertEqual(len(res.data), 2)

    def test_get_latest_pom(self):
        """Tests the route that produces the latest pomodoro"""
        pomodoro = create_pomodoro(user=self.user)
        res = self.client.get(POM_LATEST_URL)
        comparison_fields = [
            "id",
            "length",
            "created_at",
        ]
        for field in comparison_fields:
            self.assertEqual(
                res.data[field], PomodoroSerializer(pomodoro, many=False).data[field]
            )

    def test_get_latest_active_pom(self):
        """If there are two pomodoros, get the latest active one"""
        pomodoro = create_pomodoro(user=self.user)
        pomodoro2 = create_pomodoro(user=self.user)

        PomodoroActivity.objects.create(
            pomodoro=pomodoro2, activity_type=PomodoroActivity.ACTIVITY_END
        )
        pomodoro2.refresh_from_db()
        self.user.refresh_from_db()

        res = self.client.get(POM_LATEST_URL + "?active_only")
        self.assertEqual(res.data["id"], pomodoro.id)

    def test_cached_pomodoro_timeleft(self):
        """The POM_CACHE_TIMELEFT_URL route should cache based on
        Query params
        """
        pomodoro = create_pomodoro(user=self.user)
        self.client.get(
            POM_CACHE_TIMELEFT_URL + f"?pomodoro={pomodoro.id}&seconds_left={1400}"
        )
        pomodoro.refresh_from_db()
        self.assertEqual(pomodoro.seconds_left, 1400)


class PrivatePomodoroActivityApiTests(TestCase):
    """Test API requests that require authentication"""

    def setUp(self):
        self.user = create_user(
            email="test@deeptracks.com", password="testpass", name="name"
        )
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)
        self.pomodoro = create_pomodoro(self.user)

    def test_start_auto_create(self):
        """Test creating the pomodoro automatically creates the first START activity"""
        self.assertEqual(
            self.pomodoro.pomodoroactivity_set.filter(
                activity_type=PomodoroActivity.ACTIVITY_START
            ).count(),
            1,
        )

    def test_create_pomodoro_activity(self):
        """Test creating pomodoro with authenticated user"""
        payload = {
            "pomodoro": self.pomodoro.id,
            "activity_type": PomodoroActivity.ACTIVITY_END,
        }

        self.assertIsNone(self.pomodoro.ended_at)

        res = self.client.post(POM_ACTIVITY_URL, payload)

        created_pom_activity = self.user.pomodoro_set.latest("id").latest_activity

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(created_pom_activity.pomodoro.id, self.pomodoro.id)
        self.assertEqual(
            created_pom_activity.activity_type, PomodoroActivity.ACTIVITY_END
        )

        end_payload = {
            "pomodoro": self.pomodoro.id,
            "activity_type": PomodoroActivity.ACTIVITY_END,
        }

        self.client.post(POM_ACTIVITY_URL, end_payload)
        self.pomodoro.refresh_from_db()
        self.assertIsNotNone(self.pomodoro.ended_at)

    def test_get_pomodoro_activity(self):
        """Test GET only gets authenticated user's activities"""

        other_user = create_user(
            email="test2@deeptracks.com", password="testpass", name="name2"
        )

        other_user_pomodoro = Pomodoro.objects.create(
            **{"user_id": other_user.id, "length": 25, "break_length": 5}
        )

        PomodoroActivity.objects.create(
            **{
                "pomodoro": other_user_pomodoro,
                "activity_type": PomodoroActivity.ACTIVITY_START,
            }
        )

        PomodoroActivity.objects.create(
            **{
                "pomodoro": other_user_pomodoro,
                "activity_type": PomodoroActivity.ACTIVITY_END,
            }
        )

        PomodoroActivity.objects.create(
            **{
                "pomodoro": self.pomodoro,
                "activity_type": PomodoroActivity.ACTIVITY_START,
            }
        )

        PomodoroActivity.objects.create(
            **{
                "pomodoro": self.pomodoro,
                "activity_type": PomodoroActivity.ACTIVITY_END,
            }
        )

        user_pomo_activities = PomodoroActivity.objects.filter(pomodoro=self.pomodoro)

        res = self.client.get(POM_ACTIVITY_URL, {"pomodoro": self.pomodoro.id})
        serializer = PomodoroActivitySerializer(user_pomo_activities, many=True)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

        pomodoro_res = self.client.get(POM_URL)

        self.assertIsNotNone(pomodoro_res.data[0]["started_at"])
        self.assertIsNotNone(pomodoro_res.data[0]["ended_at"])

    def test_bad_get_pom_activity(self):
        """Test that not providing a pomodoro in the query params is a bad request"""
        res = self.client.get(POM_ACTIVITY_URL)

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_second_end_creation_fails(self):
        """Test that attempts to create activites on ended Pomodoro fail"""

        PomodoroActivity.objects.create(
            **{
                "pomodoro": self.pomodoro,
                "activity_type": PomodoroActivity.ACTIVITY_END,
            }
        )

        self.assertEqual(self.pomodoro.pomodoroactivity_set.all().count(), 2)

        payload = {
            "pomodoro": self.pomodoro.id,
            "activity_type": PomodoroActivity.ACTIVITY_END,
        }

        for activity in [
            PomodoroActivity.ACTIVITY_START,
            PomodoroActivity.ACTIVITY_PAUSE,
            PomodoroActivity.ACTIVITY_END,
        ]:
            payload["activity_type"] = activity
            res = self.client.post(POM_ACTIVITY_URL, payload)
            self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(
                res.data["detail"],
                "Cannot create another activity for an ended Pomodoro.",
            )

    def test_duplicate_activity_fails(self):
        """Test that attempting to create duplicate activities fails"""
        payload = {
            "pomodoro": self.pomodoro.id,
            "activity_type": PomodoroActivity.ACTIVITY_START,
        }

        res = self.client.post(POM_ACTIVITY_URL, payload)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            str(res.data["detail"]), "Cannot create 2 START activities in a row!"
        )

    def test_pomodoro_time_left_pause(self):
        """If the latest activity is a PAUSE, it should calculate
        the time left by subtracting the created_at dt from the
        activity's created at
        """
        pause_activity = PomodoroActivity.objects.create(
            pomodoro=self.pomodoro, activity_type=PomodoroActivity.ACTIVITY_PAUSE
        )
        self.assertEqual(
            self.pomodoro.time_left,
            round(
                self.pomodoro.length * 60
                - (
                    pause_activity.created_at - self.pomodoro.created_at
                ).total_seconds(),
                0,
            ),
        )
        self.assertEqual(self.pomodoro.is_paused, True)

    def test_pomodoro_time_left_running(self):
        """If it's currently running (i.e. last activity is a
        START, then calculate the time left as of now.
        """
        self.assertEqual(
            self.pomodoro.time_left,
            round(
                self.pomodoro.length * 60
                - (now_utc() - self.pomodoro.created_at).total_seconds(),
                0,
            ),
        )
        self.assertEqual(self.pomodoro.is_paused, False)

    def test_pomodoro_time_left_end(self):
        """If it's ended, the time_left should be 0"""
        PomodoroActivity.objects.create(
            pomodoro=self.pomodoro, activity_type=PomodoroActivity.ACTIVITY_END
        )
        self.assertEqual(self.pomodoro.time_left, 0)

    def test_pomodoro_time_left_start_pause(self):
        """The time_left prop should calculate the time between
        START and PAUSE activities.
        """
        time.sleep(2)
        PomodoroActivity.objects.create(
            pomodoro=self.pomodoro, activity_type=PomodoroActivity.ACTIVITY_PAUSE
        )
        time.sleep(2)
        PomodoroActivity.objects.create(
            pomodoro=self.pomodoro, activity_type=PomodoroActivity.ACTIVITY_START
        )
        time.sleep(2)
        PomodoroActivity.objects.create(
            pomodoro=self.pomodoro, activity_type=PomodoroActivity.ACTIVITY_PAUSE
        )
        expected_time_left = (self.pomodoro.length * 60) - 4
        self.assertAlmostEqual(expected_time_left, self.pomodoro.time_left, places=2)


class SuperUserPomodoroRoute(TestCase):
    def setUp(self):
        self.superuser = create_superuser()
        self.client = APIClient()
        self.client.force_authenticate(user=self.superuser)
        self.pomodoro = create_pomodoro(self.superuser)

    def test_superuser_only(self):
        """Only super users should be able to access the route"""
        user = create_user(
            email="test@deeptracks.com", password="testpass", name="name"
        )
        self.client.force_authenticate(user=user)
        res = self.client.get(POM_SUPERUSER_URL)
        self.assertEqual(res.status_code, status.HTTP_403_FORBIDDEN)

    def test_superuser_get_pomodoro(self):
        """Superuser should be able to access any pomodoro"""
        user1 = create_user(
            email="user1@deeptracks.com", password="testpass", name="user1"
        )
        user2 = create_user(
            email="user2@deeptracks.com", password="testpass", name="user2"
        )
        pom_user1 = create_pomodoro(user1)
        pom_user2 = create_pomodoro(user2)

        res1 = self.client.get(POM_SUPERUSER_URL + f"?pomodoro={pom_user1.id}")
        res2 = self.client.get(POM_SUPERUSER_URL + f"?pomodoro={pom_user2.id}")
        serializer1 = PomodoroSerializer(pom_user1, many=False)
        serializer2 = PomodoroSerializer(pom_user2, many=False)
        json1 = JSONRenderer().render(serializer1.data)
        json2 = JSONRenderer().render(serializer2.data)

        comparison_fields = [
            "id",
            "length",
            "created_at",
        ]

        res1_json = res1.json()
        serializer1_json = json.loads(json1)

        res2_json = res2.json()
        serializer2_json = json.loads(json2)

        for tup in [(res1_json, serializer1_json), (res2_json, serializer2_json)]:
            res_json, serializer_json = tup
            for field in comparison_fields:
                res_value = res_json[field]
                serializer_value = serializer_json[field]
                assert res_value == serializer_value, (
                    f"Pomodoro {field} in"
                    f"res {res_value} serializer {serializer_value}"
                )
            self.assertAlmostEqual(
                res_json["time_left"], serializer_json["time_left"], places=1
            )
